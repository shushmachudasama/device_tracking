package com.eshiksa.devicetracking.presenterimpl;

import com.eshiksa.devicetracking.pojo.plan.PlanEntity;
import com.eshiksa.devicetracking.presenter.PlanPresenter;
import com.eshiksa.devicetracking.service.PlanService;
import com.eshiksa.devicetracking.serviceimpl.activity.PlanServiceImpl;
import com.eshiksa.devicetracking.view.PlanView;

/**
 * @author by SHUSHMA on 24/4/18.
 */

public class PlanPresenterImpl implements PlanPresenter {

    private PlanView planView;

    public PlanPresenterImpl(PlanView planView) {
        this.planView = planView;
    }

    @Override
    public void onPrepareCall() {
        if (planView != null)
            planView.showProgress();
    }

    @Override
    public void onPlanCall() {

        PlanService planService = new PlanServiceImpl(this);
        planService.planCall();
    }

    @Override
    public void onPlanSuccess(PlanEntity planEntity) {
        if (planView != null) {
            planView.hideProgress();
            planView.onPlanSuccess(planEntity);
        }
    }

    @Override
    public void onPlanFailure(String message) {
        planView.onFailedMessage(message);
    }

    @Override
    public void onPlanError(int requestCode, String message) {
        if (planView != null) {
            planView.hideProgress();
            planView.onServiceError(message);
        }
    }

}
