package com.eshiksa.devicetracking.pojo.setting;

/**
 * @author by AKHILESH on 29/5/18.
 */
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
public class CheckSwitchEntity implements Parcelable
{

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("isEnabled")
    @Expose
    private Boolean isEnabled;
    public final static Parcelable.Creator<CheckSwitchEntity> CREATOR = new Creator<CheckSwitchEntity>() {


        @SuppressWarnings({
                "unchecked"
        })
        public CheckSwitchEntity createFromParcel(Parcel in) {
            return new CheckSwitchEntity(in);
        }

        public CheckSwitchEntity[] newArray(int size) {
            return (new CheckSwitchEntity[size]);
        }

    }
            ;

    protected CheckSwitchEntity(Parcel in) {
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        this.status = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.isEnabled = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
    }

    public CheckSwitchEntity() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Boolean getIsEnabled() {
        return isEnabled;
    }

    public void setIsEnabled(Boolean isEnabled) {
        this.isEnabled = isEnabled;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(message);
        dest.writeValue(status);
        dest.writeValue(isEnabled);
    }

    public int describeContents() {
        return 0;
    }

}