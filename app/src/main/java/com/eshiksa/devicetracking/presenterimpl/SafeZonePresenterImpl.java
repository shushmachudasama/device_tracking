package com.eshiksa.devicetracking.presenterimpl;

import com.eshiksa.devicetracking.pojo.safezone.SafeZoneEntity;
import com.eshiksa.devicetracking.presenter.SafeZonePresenter;
import com.eshiksa.devicetracking.service.SafeZoneService;
import com.eshiksa.devicetracking.serviceimpl.activity.SafeZoneserviceImpl;
import com.eshiksa.devicetracking.view.SafezoneView;

/**
 * @author by SHUSHMA on 12/5/18.
 */

public class SafeZonePresenterImpl implements SafeZonePresenter {

    private SafezoneView safezoneView;

    public SafeZonePresenterImpl(SafezoneView safezoneView) {
        this.safezoneView = safezoneView;
    }

    @Override
    public void onPrepareCall() {
        if (safezoneView != null)
            safezoneView.showProgress();
    }

    @Override
    public void onAddSafeZoneCall(SafeZoneEntity safeZoneEntity) {
        SafeZoneService safeZoneService = new SafeZoneserviceImpl(this);
        safeZoneService.addSafeZoneCall(safeZoneEntity);
    }

    @Override
    public void onAddSafeZoneSuccess(SafeZoneEntity safeZoneEntity) {
        if (safezoneView != null) {
            safezoneView.hideProgress();
            safezoneView.onSafeZoneSuccess(safeZoneEntity);
        }
    }

    @Override
    public void onSafeZoneListCall(String username, String deviceId) {
        SafeZoneService safeZoneService = new SafeZoneserviceImpl(this);
        safeZoneService.safeZoneListCall(username, deviceId);
    }

    @Override
    public void onSafeZoneListSuccess(SafeZoneEntity safeZoneEntity) {
        if (safezoneView != null) {
            safezoneView.hideProgress();
            safezoneView.onSafeZoneListSuccess(safeZoneEntity);
        }
    }

    @Override
    public void onSafeZoneFailure(String message) {
        safezoneView.onFailedMessage(message);
    }

    @Override
    public void onSafeZoneError(int requestCode, String message) {
        if (safezoneView != null) {
            safezoneView.hideProgress();
            safezoneView.onServiceError(message);
        }
    }
}
