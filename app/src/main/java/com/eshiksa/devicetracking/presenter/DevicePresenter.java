package com.eshiksa.devicetracking.presenter;

import com.eshiksa.devicetracking.pojo.device.device_list.DeviceListMainEntity;

/**
 * @author by SHUSHMA on 11/5/18.
 */

public interface DevicePresenter {

    void onPrepareCall();

    void onDeviceListCall(String username);

    void onDeviceListSuccess(DeviceListMainEntity deviceListMainEntity);

    void onDeviceListFailure(String message);

    void onDeviceListError(int requestCode, String message);

}
