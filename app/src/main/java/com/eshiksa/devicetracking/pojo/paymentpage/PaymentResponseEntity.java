package com.eshiksa.devicetracking.pojo.paymentpage;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author by AKHILESH on 3/5/18.
 */
public class PaymentResponseEntity implements Parcelable
{

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("redirectUrl")
    @Expose
    private String redirectUrl;
    public final static Parcelable.Creator<PaymentResponseEntity> CREATOR = new Creator<PaymentResponseEntity>() {


        @SuppressWarnings({
                "unchecked"
        })
        public PaymentResponseEntity createFromParcel(Parcel in) {
            return new PaymentResponseEntity(in);
        }

        public PaymentResponseEntity[] newArray(int size) {
            return (new PaymentResponseEntity[size]);
        }

    }
            ;

    protected PaymentResponseEntity(Parcel in) {
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        this.status = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.redirectUrl = ((String) in.readValue((String.class.getClassLoader())));
    }

    public PaymentResponseEntity() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(message);
        dest.writeValue(status);
        dest.writeValue(redirectUrl);
    }

    public int describeContents() {
        return 0;
    }

}