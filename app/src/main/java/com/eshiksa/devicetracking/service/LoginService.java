package com.eshiksa.devicetracking.service;

/**
 * @author by SHUSHMA on 23/4/18.
 */

public interface LoginService {

    void onLoginServiceCall(String username, String password);

    void onEmailServiceCall(String email);

    void onSignUpServiceCall(String username, String password, String email, String contact, String address);

}
