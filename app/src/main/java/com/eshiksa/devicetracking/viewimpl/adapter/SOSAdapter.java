package com.eshiksa.devicetracking.viewimpl.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.eshiksa.devicetracking.R;
import com.eshiksa.devicetracking.pojo.device.device_list.DeviceList;
import com.eshiksa.devicetracking.viewimpl.activity.SOSCallingActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author by AKHILESH on 27/8/18.
 */
public class SOSAdapter extends RecyclerView.Adapter<SOSAdapter.ViewHolder> {
    private Context context;
    private OnSosCallClickListner onSosCallClickListner;
    private  List<DeviceList> mList;

    public SOSAdapter(Context context, OnSosCallClickListner onSosCallClickListner, List<DeviceList> mList) {
        this.context = context;
        this.onSosCallClickListner = onSosCallClickListner;
        this.mList = mList;
    }

    public interface OnSosCallClickListner{
        void onDeviceClick(String deviceNumber);
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.device_items, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {


        holder.txtSwitch.setVisibility(View.GONE);

    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtDeviceId)
        TextView txtDeviceId;
        @BindView(R.id.txtDeviceName)
        TextView txtDeviceName;
        @BindView(R.id.txtSwitch)
        TextView txtSwitch;
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
