package com.eshiksa.devicetracking.viewimpl.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.eshiksa.devicetracking.R;
import com.eshiksa.devicetracking.pojo.safezone.Devicezone;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author by SHUSHMA on 14/5/18.
 */

public class SafeZoneAdapter extends RecyclerView.Adapter<SafeZoneAdapter.SafeZoneHolder> {

    private List<Devicezone> devicezones;
    private Activity mActivity;
    private OnZoneClickListener onZoneClickListener;

    @NonNull
    @Override
    public SafeZoneHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.content_safe_zone, parent, false);
        return new SafeZoneHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SafeZoneHolder holder, final int position) {

        final Devicezone devicezone = devicezones.get(position);
        holder.distance.setText(devicezone.getDistance() + "m");
        holder.placeNickName.setText(devicezone.getAlternatePlaceName());
        holder.place.setText(devicezone.getPlaceName());

        holder.imgModifySafeZone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onZoneClickListener.onZoneClick(devicezone, position);

            }
        });

        holder.imgDelZone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onZoneClickListener.onDeleteZoneClick(devicezone, position);

            }
        });

    }

    @Override
    public int getItemCount() {
        return devicezones.size();
    }

    public interface OnZoneClickListener {
        void onZoneClick(Devicezone devicezone, int position);
        void onDeleteZoneClick(Devicezone devicezone, int position);
    }

    public SafeZoneAdapter(List<Devicezone> devicezones, Activity mActivity, OnZoneClickListener onZoneClickListener) {
        this.devicezones = devicezones;
        this.mActivity = mActivity;
        this.onZoneClickListener = onZoneClickListener;
    }

    public class SafeZoneHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.placeNickName)
        TextView placeNickName;
        @BindView(R.id.place)
        TextView place;
        @BindView(R.id.distance)
        TextView distance;
        @BindView(R.id.imgModifySafeZone)
        ImageView imgModifySafeZone;
        @BindView(R.id.imgDelZone)
        ImageView imgDelZone;

        public SafeZoneHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
