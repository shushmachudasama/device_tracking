
package com.eshiksa.devicetracking.pojo.safezone;

import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SafeZoneEntity implements Parcelable {

    @SerializedName("userID")
    @Expose
    private String userID;
    @SerializedName("safeZoneId")
    @Expose
    private Integer safeZoneId;
    @SerializedName("deviceID")
    @Expose
    private String deviceID;
    @SerializedName("deviceNumber")
    @Expose
    private String deviceNumber;
    @SerializedName("placeName")
    @Expose
    private String placeName;
    @SerializedName("alternatePlaceName")
    @Expose
    private String alternatePlaceName;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("distance")
    @Expose
    private String distance;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("devicezones")
    @Expose
    private List<Devicezone> devicezones = null;
    public final static Creator<SafeZoneEntity> CREATOR = new Creator<SafeZoneEntity>() {

        @SuppressWarnings({
                "unchecked"
        })
        public SafeZoneEntity createFromParcel(Parcel in) {
            return new SafeZoneEntity(in);
        }

        public SafeZoneEntity[] newArray(int size) {
            return (new SafeZoneEntity[size]);
        }

    };

    protected SafeZoneEntity(Parcel in) {
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        this.placeName = ((String) in.readValue((String.class.getClassLoader())));
        this.alternatePlaceName = ((String) in.readValue((String.class.getClassLoader())));
        this.deviceNumber = ((String) in.readValue((String.class.getClassLoader())));
        this.status = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        in.readList(this.devicezones, (Devicezone.class.getClassLoader()));
        this.username = ((String) in.readValue((String.class.getClassLoader())));
        this.latitude = ((String) in.readValue((String.class.getClassLoader())));
        this.longitude = ((String) in.readValue((String.class.getClassLoader())));
        this.distance = ((String) in.readValue((String.class.getClassLoader())));
        this.deviceID = ((String) in.readValue((String.class.getClassLoader())));
        this.userID = ((String) in.readValue((String.class.getClassLoader())));
        this.safeZoneId = ((Integer) in.readValue((Integer.class.getClassLoader())));

    }

    public SafeZoneEntity() {
    }

    public String getPlaceName() {
        return placeName;
    }

    public void setPlaceName(String placeName) {
        this.placeName = placeName;
    }

    public String getAlternatePlaceName() {
        return alternatePlaceName;
    }

    public void setAlternatePlaceName(String alternatePlaceName) {
        this.alternatePlaceName = alternatePlaceName;
    }

    public String getDeviceNumber() {
        return deviceNumber;
    }

    public void setDeviceNumber(String deviceNumber) {
        this.deviceNumber = deviceNumber;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public Integer getSafeZoneId() {
        return safeZoneId;
    }

    public void setSafeZoneId(Integer safeZoneId) {
        this.safeZoneId = safeZoneId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getDeviceID() {
        return deviceID;
    }

    public void setDeviceID(String deviceID) {
        this.deviceID = deviceID;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<Devicezone> getDevicezones() {
        return devicezones;
    }

    public void setDevicezones(List<Devicezone> devicezones) {
        this.devicezones = devicezones;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(message);
        dest.writeValue(placeName);
        dest.writeValue(alternatePlaceName);
        dest.writeValue(deviceNumber);
        dest.writeValue(status);
        dest.writeList(devicezones);
        dest.writeValue(userID);
        dest.writeValue(deviceID);
        dest.writeValue(latitude);
        dest.writeValue(longitude);
        dest.writeValue(distance);
        dest.writeValue(username);
        dest.writeValue(safeZoneId);
    }

    public int describeContents() {
        return 0;
    }

}
