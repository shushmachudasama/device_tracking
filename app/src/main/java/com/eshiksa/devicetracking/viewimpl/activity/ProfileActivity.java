package com.eshiksa.devicetracking.viewimpl.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.eshiksa.devicetracking.R;
import com.eshiksa.devicetracking.util.Constant;
import com.eshiksa.devicetracking.util.SharePrefrancClass;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ProfileActivity extends AppCompatActivity {

    private String username;

    @BindView(R.id.ed_user)
    EditText edUser;
    @BindView(R.id.ed_name)
    EditText edName;
    @BindView(R.id.ed_phone)
    EditText edPhone;
    @BindView(R.id.ed_email)
    EditText edEmail;
    @BindView(R.id.radioGender)
    RadioGroup radioGender;
    @BindView(R.id.radioMale)
    RadioButton radioMale;
    @BindView(R.id.radioFemale)
    RadioButton radioFemale;
    @BindView(R.id.imageBack)
    ImageView imageBack;
    @BindView(R.id.btnProfileOk)
    Button btnProfileOk;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        ButterKnife.bind(this);
        username = SharePrefrancClass.getInstance(getApplicationContext()).getPref(Constant.USERNAME);
        edUser.setText(username);
    }

    @OnClick({R.id.btnProfileOk, R.id.imageBack})
    public void onOkClick(View view) {
        switch (view.getId()) {

            case R.id.btnProfileOk:
                updatePersonalInfo();
//                Toast.makeText(this, "Personal info ok.", Toast.LENGTH_SHORT).show();
                break;

            case R.id.imageBack:
                onBackPressed();
                break;
        }
    }

    /**
     * Method for updating personal info of app user
     */
    private void updatePersonalInfo() {
        edUser.getText().toString();
        edEmail.getText().toString();
        edPhone.getText().toString();
        edName.getText().toString();
        String gender;
        int genderId = radioGender.getCheckedRadioButtonId();
        if (genderId == R.id.radioMale) {
            gender = "Male";
        } else if (genderId == R.id.radioFemale) {
            gender = "Female";
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
