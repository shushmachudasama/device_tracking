
package com.eshiksa.devicetracking.pojo;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginEntity implements Parcelable {

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("username")
    @Expose
    private String username;
    public final static Creator<LoginEntity> CREATOR = new Creator<LoginEntity>() {


        @SuppressWarnings({
                "unchecked"
        })
        public LoginEntity createFromParcel(Parcel in) {
            return new LoginEntity(in);
        }

        public LoginEntity[] newArray(int size) {
            return (new LoginEntity[size]);
        }

    };

    protected LoginEntity(Parcel in) {
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        this.status = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.username = ((String) in.readValue((String.class.getClassLoader())));
    }

    public LoginEntity() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(message);
        dest.writeValue(status);
        dest.writeValue(username);
    }

    public int describeContents() {
        return 0;
    }

}
