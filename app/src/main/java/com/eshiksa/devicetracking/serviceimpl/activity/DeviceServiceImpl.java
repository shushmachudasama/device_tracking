package com.eshiksa.devicetracking.serviceimpl.activity;

import com.eshiksa.devicetracking.pojo.device.device_list.DeviceListMainEntity;
import com.eshiksa.devicetracking.presenter.DevicePresenter;
import com.eshiksa.devicetracking.service.DeviceService;
import com.eshiksa.devicetracking.serviceimpl.ApiClient;
import com.eshiksa.devicetracking.serviceimpl.ApiInterface;

import java.net.HttpURLConnection;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * @author by SHUSHMA on 11/5/18.
 */

public class DeviceServiceImpl implements DeviceService {
    
    private DevicePresenter devicePresenter;

    public DeviceServiceImpl(DevicePresenter devicePresenter) {
        this.devicePresenter = devicePresenter;
    }

    @Override
    public void onDeviceListServiceCall(String username) {
        devicePresenter.onPrepareCall();
        ApiInterface apiInterface = ApiClient.getClient(null).create(ApiInterface.class);
        HashMap<String, Object> map = new HashMap<>();
        map.put("username", username);
        Call<DeviceListMainEntity> deviceListEntityCall = apiInterface.getDeviceList(map);
        deviceListEntityCall.enqueue(new Callback<DeviceListMainEntity>() {
            @Override
            public void onResponse(Call<DeviceListMainEntity> call, Response<DeviceListMainEntity> response) {

                if (response.body() != null) {

                    DeviceListMainEntity deviceListEntity = response.body();
                    if (deviceListEntity.getStatus()) {
                        devicePresenter.onDeviceListSuccess(deviceListEntity);
                    } else {
                        devicePresenter.onDeviceListFailure(deviceListEntity.getMessages());
                    }

                } else {
                    devicePresenter.onDeviceListFailure(response.message());
                }

            }

            @Override
            public void onFailure(Call<DeviceListMainEntity> call, Throwable t) {
                devicePresenter.onDeviceListError(HttpURLConnection.HTTP_INTERNAL_ERROR, "Oops! Some problem seems to have happened. Please try again");
            }

        });

    }
}
