package com.eshiksa.devicetracking.viewimpl.activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.eshiksa.devicetracking.R;
import com.eshiksa.devicetracking.pojo.device.AddDeviceEntity;
import com.eshiksa.devicetracking.pojo.device.device_list.DeviceList;
import com.eshiksa.devicetracking.pojo.device.device_list.DeviceListMainEntity;
import com.eshiksa.devicetracking.presenter.DevicePresenter;
import com.eshiksa.devicetracking.presenterimpl.DevicePresenterImpl;
import com.eshiksa.devicetracking.serviceimpl.ApiClient;
import com.eshiksa.devicetracking.serviceimpl.ApiInterface;
import com.eshiksa.devicetracking.util.Constant;
import com.eshiksa.devicetracking.util.SharePrefrancClass;
import com.eshiksa.devicetracking.util.ShowAlertDialog;
import com.eshiksa.devicetracking.view.DeviceView;
import com.eshiksa.devicetracking.viewimpl.adapter.DeviceAdapter;
import com.eshiksa.devicetracking.viewimpl.fragment.ProgressDialogFragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DeviceActivity extends AppCompatActivity implements DeviceView {

    private static final String TAG = "DEVICE ACTIVITY";
    private List<DeviceList> mList;

    @BindView(R.id.recycler_devices)
    RecyclerView recyclerDevices;
    @BindView(R.id.imageBack)
    ImageView imageBack;
    @BindView(R.id.addDevice)
    LinearLayout addDevice;
    @BindView(R.id.modifyNickname)
    LinearLayout modifyNickname;

    EditText mEdWatchID, mEdNickName, mEdMobile;
    Dialog dialog;

    private final int BAR_CODE_REQUEST = 101;
    boolean check = false;
    private ProgressDialogFragment progressDialogFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device);
        ButterKnife.bind(this);
        progressDialogFragment = new ProgressDialogFragment();
        mList = new ArrayList<>();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getDeviceList();
    }

    private void getDeviceList() {
        String username = SharePrefrancClass.getInstance(this).getPref(Constant.USERNAME);
        DevicePresenter devicePresenter = new DevicePresenterImpl(this);
        devicePresenter.onDeviceListCall(username);
    }

    @OnClick({R.id.imageBack, R.id.modifyNickname, R.id.addDevice})
    public void onClickBtn(View view) {

        switch (view.getId()) {

            case R.id.imageBack:
                onBackPressed();
                break;

            case R.id.addDevice:
                addDeviceTask();
                break;

            case R.id.modifyNickname:
                modifyNicknameTask();
                break;

//            case R.id.txtEdit:
//                editDeviceList();
//                break;

        }
    }

    /**
     * Method to perform edit device list task
     */
    private void editDeviceList() {
    }

    /**
     * Method to add new device in app for tracking
     */
    private void addDeviceTask() {

        dialog = new Dialog(DeviceActivity.this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.add_device);
        dialog.setCancelable(false);
        dialog.show();

        mEdWatchID = dialog.findViewById(R.id.ed_watch_id);
        mEdNickName = dialog.findViewById(R.id.ed_nickname);
        mEdMobile = dialog.findViewById(R.id.ed_mobile);
        ImageView cancel = dialog.findViewById(R.id.cancel);
        ImageView imgScan = dialog.findViewById(R.id.img_scan);
        Button btn_save = dialog.findViewById(R.id.btn_save);

        imgScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scanDeviceCode();
            }
        });

        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callAddDevice();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }

    private void callAddDevice() {
        if (!isValid()) {
            return;
        } else {

            if (!isValidMobile(mEdMobile.getText().toString())) {
                check = false;
            } else {
                check = true;
                ApiInterface apiInterface = ApiClient.getClient(null).create(ApiInterface.class);
                String username = SharePrefrancClass.getInstance(this).getPref(Constant.USERNAME);
                HashMap<String, Object> params = new HashMap<String, Object>();
                params.put("deviceNumber", mEdWatchID.getText().toString());
                params.put("nickName", mEdNickName.getText().toString());
                params.put("mobileNumber", mEdMobile.getText().toString());
                params.put("username", username);

                Call<AddDeviceEntity> call = apiInterface.addDeviceEntity(params);
                call.enqueue(new Callback<AddDeviceEntity>() {
                    @Override
                    public void onResponse(Call<AddDeviceEntity> call, Response<AddDeviceEntity> response) {
                        if (response.body() != null) {
                            AddDeviceEntity entity = response.body();
                            dialog.dismiss();
                            ShowAlertDialog.showAlert(DeviceActivity.this, entity.getMessages(), "OK");

                        } else {
                            ShowAlertDialog.showAlert(DeviceActivity.this, response.body().getMessages(), "OK");

                        }
                    }

                    @Override
                    public void onFailure(Call<AddDeviceEntity> call, Throwable t) {
                        ShowAlertDialog.showAlert(DeviceActivity.this, getApplication().getResources().getString(R.string.message_oops), "OK");

                    }
                });
            }
        }
    }

    private boolean isValidMobile(String phone) {
        check = false;
        if (!Pattern.matches("[a-zA-Z]+", phone)) {
            if (phone.length() < 6 || phone.length() > 13) {
                // if(phone.length() != 10) { 
                check = false;
                Toast.makeText(this, "Not Valid Number !!!", Toast.LENGTH_SHORT).show();
            } else {
                check = true;
            }
        } else {
            check = false;
        }
        return check;
    }

    private boolean isValid() {
        boolean isValid = true;

        String message = "";
        if (mEdWatchID.getText().toString().isEmpty()) {
            isValid = false;
            message = "Please enter device no.";
        } else if (mEdNickName.getText().toString().isEmpty()) {
            isValid = false;
            message = "Please enter nickname";
        } else if (mEdMobile.getText().toString().isEmpty()) {
            isValid = false;
            message = "Please enter mobile no.";
        }

        if (!isValid) {
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();

        }
        return isValid;
    }

    /**
     * Method to scan code on device
     */
    private void scanDeviceCode() {
        Intent scanIntent = new Intent(this, ScannerActivity.class);
        startActivityForResult(scanIntent, BAR_CODE_REQUEST);
    }

    /**
     * Method to modify nickname of device
     */
    private void modifyNicknameTask() {

        if (mList.size() > 0) {
            Intent intent = new Intent(DeviceActivity.this, ModifyDeviceNameActivity.class);
            intent.putParcelableArrayListExtra("mList", (ArrayList<? extends Parcelable>) mList);
            startActivity(intent);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case BAR_CODE_REQUEST:
                if (resultCode == RESULT_OK) {
                    String scanData = data.getStringExtra("SCAN_DATA");
                    mEdWatchID.setText(scanData);

                }
                break;
        }
    }

    @Override
    public void onDeviceListSuccess(DeviceListMainEntity entity) {
        mList = entity.getDeviceList();

        if (mList.size() > 0) {
            DeviceAdapter deviceAdapter = new DeviceAdapter(this, mList);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(DeviceActivity.this);
            recyclerDevices.setLayoutManager(layoutManager);
            recyclerDevices.setAdapter(deviceAdapter);
        }

    }

    @Override
    public void onFailedMessage(String message) {
        ShowAlertDialog.showAlert(DeviceActivity.this, message, "OK");
        hideProgress();
    }

    @Override
    public void onServiceError(String status) {
        ShowAlertDialog.showAlert(DeviceActivity.this, getApplication().getResources().getString(R.string.message_oops), "OK");
        hideProgress();
    }

    @Override
    public void showProgress() {
        if (progressDialogFragment != null)
            progressDialogFragment.show(getFragmentManager(), "Loading...");
    }

    @Override
    public void hideProgress() {
        if (progressDialogFragment != null)
            progressDialogFragment.dismiss();
    }
}
