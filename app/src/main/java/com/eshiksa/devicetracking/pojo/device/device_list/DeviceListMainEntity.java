package com.eshiksa.devicetracking.pojo.device.device_list;

/**
 * @author by AKHILESH on 9/5/18.
 */

import java.util.ArrayList;
import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DeviceListMainEntity implements Parcelable {

    @SerializedName("messages")
    @Expose
    private String messages;
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("deviceList")
    @Expose
    private List<DeviceList> deviceList = new ArrayList<>();
    public final static Parcelable.Creator<DeviceListMainEntity> CREATOR = new Creator<DeviceListMainEntity>() {


        @SuppressWarnings({
                "unchecked"
        })
        public DeviceListMainEntity createFromParcel(Parcel in) {
            return new DeviceListMainEntity(in);
        }

        public DeviceListMainEntity[] newArray(int size) {
            return (new DeviceListMainEntity[size]);
        }

    };

    protected DeviceListMainEntity(Parcel in) {
        this.messages = ((String) in.readValue((String.class.getClassLoader())));
        this.status = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        in.readList(this.deviceList, (DeviceList.class.getClassLoader()));
    }

    public DeviceListMainEntity() {
    }

    public String getMessages() {
        return messages;
    }

    public void setMessages(String messages) {
        this.messages = messages;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<DeviceList> getDeviceList() {
        return deviceList;
    }

    public void setDeviceList(List<DeviceList> deviceList) {
        this.deviceList = deviceList;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(messages);
        dest.writeValue(status);
        dest.writeList(deviceList);
    }

    public int describeContents() {
        return 0;
    }

}
