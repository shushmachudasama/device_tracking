
package com.eshiksa.devicetracking.pojo.order;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ListOfOrder implements Parcelable {

    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("orderId")
    @Expose
    private String orderId;
    @SerializedName("createdDate")
    @Expose
    private String createdDate;
    @SerializedName("txnAmount")
    @Expose
    private Integer txnAmount;
    @SerializedName("numberOfDevices")
    @Expose
    private String numberOfDevices;
    @SerializedName("orderStatus")
    @Expose
    private String orderStatus;
    public final static Creator<ListOfOrder> CREATOR = new Creator<ListOfOrder>() {


        @SuppressWarnings({
                "unchecked"
        })
        public ListOfOrder createFromParcel(Parcel in) {
            return new ListOfOrder(in);
        }

        public ListOfOrder[] newArray(int size) {
            return (new ListOfOrder[size]);
        }

    };

    protected ListOfOrder(Parcel in) {
        this.username = ((String) in.readValue((String.class.getClassLoader())));
        this.orderId = ((String) in.readValue((String.class.getClassLoader())));
        this.createdDate = ((String) in.readValue((String.class.getClassLoader())));
        this.txnAmount = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.numberOfDevices = ((String) in.readValue((String.class.getClassLoader())));
        this.orderStatus = ((String) in.readValue((String.class.getClassLoader())));
    }

    public ListOfOrder() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public Integer getTxnAmount() {
        return txnAmount;
    }

    public void setTxnAmount(Integer txnAmount) {
        this.txnAmount = txnAmount;
    }

    public String getNumberOfDevices() {
        return numberOfDevices;
    }

    public void setNumberOfDevices(String numberOfDevices) {
        this.numberOfDevices = numberOfDevices;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(username);
        dest.writeValue(orderId);
        dest.writeValue(createdDate);
        dest.writeValue(txnAmount);
        dest.writeValue(numberOfDevices);
        dest.writeValue(orderStatus);
    }

    public int describeContents() {
        return 0;
    }

}
