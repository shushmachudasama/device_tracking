package com.eshiksa.devicetracking.service;

import com.eshiksa.devicetracking.pojo.safezone.SafeZoneEntity;

/**
 * @author by SHUSHMA on 16/5/18.
 */

public interface ModifyZoneService {

    void modifyZoneCall(SafeZoneEntity safeZoneEntity);
    void deleteZoneCall(String zoneId);

}
