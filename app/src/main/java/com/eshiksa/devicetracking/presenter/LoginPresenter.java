package com.eshiksa.devicetracking.presenter;

import com.eshiksa.devicetracking.pojo.LoginEntity;

/**
 * @author by SHUSHMA on 23/4/18.
 */

public interface LoginPresenter {

    void onPrepareCall();

    void onLoginCall(String username, String password);

    void onLoginSuccess(LoginEntity loginEntity);

    void onLoginFailure(String message);

    void onLoginError(int requestCode, String message);

    void onSignUpCall(String username, String password, String email, String contact, String address);

    void onSignUpSuccess(LoginEntity loginEntity);

    void onEmailCall(String email);

    void onEmailSuccess(LoginEntity loginEntity);
}
