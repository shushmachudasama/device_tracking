package com.eshiksa.devicetracking.viewimpl.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.eshiksa.devicetracking.R;
import com.eshiksa.devicetracking.pojo.device.device_list.DeviceList;
import com.eshiksa.devicetracking.util.Constant;
import com.eshiksa.devicetracking.viewimpl.activity.HomeActivity;

import java.util.List;

/**
 * @author by AKHILESH on 12/5/18.
 */
public class GridDeviceAdapter extends BaseAdapter{

    Activity activity;
    List<DeviceList> mList;
    OnDeviceClickListener onDeviceClickListener;

    public GridDeviceAdapter(Activity homeActivity, List<DeviceList> mList, OnDeviceClickListener onDeviceClickListener) {
        this.activity = homeActivity;
        this.mList = mList;
        this.onDeviceClickListener = onDeviceClickListener;
    }

    public interface OnDeviceClickListener {
        void onDeviceClick(String deviceNumber, String nickname);
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = activity.getLayoutInflater();
        final View view = inflater.inflate(R.layout.home_device_list_item, parent, false);

        final TextView txtDeviceNickName = view.findViewById(R.id.txtDeviceNickName);
        final ImageView imgDevice = view.findViewById(R.id.imgDevice);
        txtDeviceNickName.setText(mList.get(position).getNickName());

        imgDevice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgDevice.setBackgroundColor(activity.getResources().getColor(R.color.colorAccent));
                onDeviceClickListener.onDeviceClick(mList.get(position).getDeviceNumber(), mList.get(position).getNickName());
            }
        });
        return view;

    }
}
