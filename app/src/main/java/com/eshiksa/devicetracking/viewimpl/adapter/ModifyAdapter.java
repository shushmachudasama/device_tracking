package com.eshiksa.devicetracking.viewimpl.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.eshiksa.devicetracking.R;
import com.eshiksa.devicetracking.pojo.device.device_list.DeviceList;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author by SHUSHMA on 7/5/18.
 */

public class ModifyAdapter extends RecyclerView.Adapter<ModifyAdapter.ModifyHolder> {

    private List<DeviceList> mList;
    private Activity activity;

    private OnImgEditClickListener onImgEditClickListener;

    public interface OnImgEditClickListener {
        void onImgEditClick(DeviceList deviceListEntity);
    }

    public ModifyAdapter(List<DeviceList> mList, Activity activity,OnImgEditClickListener onImgEditClickListener) {
        this.mList = mList;
        this.activity = activity;
        this.onImgEditClickListener = onImgEditClickListener;
    }

    @NonNull
    @Override
    public ModifyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.modify_nickname, parent, false);
        return new ModifyHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ModifyHolder holder, int position) {

        final DeviceList deviceList = mList.get(position);
        holder.editNickname.setText(deviceList.getNickName());
        holder.txtDeviceId.setText(deviceList.getDeviceNumber());

        holder.imgEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onImgEditClickListener.onImgEditClick(deviceList);
            }
        });

    }

    private void opnDilgBoxModifyNickName(DeviceList deviceList) {

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ModifyHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.editNickname)
        TextView editNickname;
        @BindView(R.id.deviceId)
        TextView txtDeviceId;
        @BindView(R.id.imgEdit)
        ImageView imgEdit;

        public ModifyHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
