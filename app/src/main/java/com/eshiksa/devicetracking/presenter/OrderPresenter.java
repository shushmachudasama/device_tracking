package com.eshiksa.devicetracking.presenter;

import com.eshiksa.devicetracking.pojo.LoginEntity;
import com.eshiksa.devicetracking.pojo.order.OrderEntity;

/**
 * @author by SHUSHMA on 24/4/18.
 */

public interface OrderPresenter {


    void onPrepareCall();

    void onOrderCall(OrderEntity orderEntity);

    void onOrderSuccess(OrderEntity orderEntity);

    void onOrderFailure(String message);

    void onOrderError(int requestCode, String message);

}
