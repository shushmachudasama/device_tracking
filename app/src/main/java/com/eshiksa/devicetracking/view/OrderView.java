package com.eshiksa.devicetracking.view;

import com.eshiksa.devicetracking.pojo.order.OrderEntity;

/**
 * @author by SHUSHMA on 24/4/18.
 */

public interface OrderView extends CommonView {

    void onOrderSuccess(OrderEntity orderEntity);

    void onFailedMessage(String message);

    void onServiceError(String message);
}
