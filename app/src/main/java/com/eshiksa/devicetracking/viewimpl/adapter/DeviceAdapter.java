package com.eshiksa.devicetracking.viewimpl.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.eshiksa.devicetracking.R;
import com.eshiksa.devicetracking.pojo.device.device_list.DeviceList;
import com.eshiksa.devicetracking.viewimpl.activity.DeviceActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author by SHUSHMA on 5/5/18.
 */

public class DeviceAdapter extends RecyclerView.Adapter<DeviceAdapter.DeviceHolder> {
    private Activity activity;
    private List<DeviceList> mList;

    public DeviceAdapter(Activity deviceActivity, List<DeviceList> mList) {
        this.activity = deviceActivity;
        this.mList = mList;
    }

    @NonNull
    @Override
    public DeviceHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.device_items, parent, false);
        return new DeviceHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DeviceHolder holder, int position) {
      DeviceList entity = mList.get(position);
        holder.txtDeviceName.setText(entity.getNickName());
        holder.txtDeviceId.setText(entity.getDeviceNumber());


    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class DeviceHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txtDeviceId)
        TextView txtDeviceId;
        @BindView(R.id.txtDeviceName)
        TextView txtDeviceName;

        public DeviceHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
