package com.eshiksa.devicetracking.viewimpl.fragment;

import android.app.DialogFragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.eshiksa.devicetracking.R;

/**
 * @author by SHUSHMA on 23/4/18.
 */

public class ProgressDialogFragment extends DialogFragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        getDialog().requestWindowFeature(STYLE_NO_TITLE);
        setCancelable(false);
        View view = inflater.inflate(R.layout.dialog_progress, container, false);
        return view;
    }

    @Override
    public void show(FragmentManager manager, String title) {
        try {
            super.show(manager, title);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void dismiss() {

        try {
            super.dismiss();
        } catch (NullPointerException | IllegalStateException np) {
            np.printStackTrace();
        }
    }
}
