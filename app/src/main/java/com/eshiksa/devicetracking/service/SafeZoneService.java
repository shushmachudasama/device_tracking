package com.eshiksa.devicetracking.service;

import com.eshiksa.devicetracking.pojo.safezone.SafeZoneEntity;

/**
 * @author by SHUSHMA on 12/5/18.
 */

public interface SafeZoneService {

    void addSafeZoneCall(SafeZoneEntity safeZoneEntity);

    void safeZoneListCall(String username, String deviceId);

}
