package com.eshiksa.devicetracking.util;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

/**
 * @author by SHUSHMA on 23/4/18.
 */

public class ShowAlertDialog {

    public static void showAlert(Context context, String title, String buttonTitle){
        if (buttonTitle == null || buttonTitle.isEmpty()){
            buttonTitle = "OK";
        }

        AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
        builder1.setCancelable(false);
        builder1.setPositiveButton(
                "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        builder1.setMessage(title);
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

}
