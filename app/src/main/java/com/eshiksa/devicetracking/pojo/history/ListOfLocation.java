
package com.eshiksa.devicetracking.pojo.history;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ListOfLocation implements Parcelable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("deviceNumber")
    @Expose
    private String deviceNumber;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("lattitude")
    @Expose
    private String lattitude;
    @SerializedName("addressId")
    @Expose
    private Integer addressId;
    @SerializedName("userName")
    @Expose
    private String userName;
    @SerializedName("createdDate")
    @Expose
    private String createdDate;
    public final static Creator<ListOfLocation> CREATOR = new Creator<ListOfLocation>() {


        @SuppressWarnings({
                "unchecked"
        })
        public ListOfLocation createFromParcel(Parcel in) {
            return new ListOfLocation(in);
        }

        public ListOfLocation[] newArray(int size) {
            return (new ListOfLocation[size]);
        }

    };

    protected ListOfLocation(Parcel in) {
        this.id = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.deviceNumber = ((String) in.readValue((String.class.getClassLoader())));
        this.longitude = ((String) in.readValue((String.class.getClassLoader())));
        this.lattitude = ((String) in.readValue((String.class.getClassLoader())));
        this.addressId = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.userName = ((String) in.readValue((String.class.getClassLoader())));
        this.createdDate = ((String) in.readValue((String.class.getClassLoader())));
    }

    public ListOfLocation() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDeviceNumber() {
        return deviceNumber;
    }

    public void setDeviceNumber(String deviceNumber) {
        this.deviceNumber = deviceNumber;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLattitude() {
        return lattitude;
    }

    public void setLattitude(String lattitude) {
        this.lattitude = lattitude;
    }

    public Integer getAddressId() {
        return addressId;
    }

    public void setAddressId(Integer addressId) {
        this.addressId = addressId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(deviceNumber);
        dest.writeValue(longitude);
        dest.writeValue(lattitude);
        dest.writeValue(addressId);
        dest.writeValue(userName);
        dest.writeValue(createdDate);
    }

    public int describeContents() {
        return 0;
    }

}
