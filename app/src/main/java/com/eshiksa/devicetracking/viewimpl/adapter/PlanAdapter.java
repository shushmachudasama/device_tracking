package com.eshiksa.devicetracking.viewimpl.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.eshiksa.devicetracking.R;
import com.eshiksa.devicetracking.pojo.plan.DevicePlansVO;

import java.util.List;

/**
 * @author by SHUSHMA on 25/4/18.
 */

public class PlanAdapter extends BaseAdapter {

    private Activity mContext;
    private List<DevicePlansVO> mPlanList;
    private OnPurchaseClickListener onPurchaseClickListener;

    public interface OnPurchaseClickListener {
        void onPurchaseClick(Integer id);
    }

    // 1
    public PlanAdapter(Activity mContext, List<DevicePlansVO> mPlanList, OnPurchaseClickListener onPurchaseClickListener) {
        this.mContext = mContext;
        this.mPlanList = mPlanList;
        this.onPurchaseClickListener = onPurchaseClickListener;
    }

    // 2
    @Override
    public int getCount() {
        return mPlanList.size();
    }

    // 3
    @Override
    public long getItemId(int position) {
        return 0;
    }

    // 4
    @Override
    public Object getItem(int position) {
        return null;
    }

// 5
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = mContext.getLayoutInflater();
        final View planView = inflater.inflate(R.layout.content_plan, parent, false);

        final Button btnPurchase = planView.findViewById(R.id.btnPurchase);
        final TextView tvPlanType = planView.findViewById(R.id.tvPlanType);
        final TextView tvDiscount = planView.findViewById(R.id.tvDiscount);
        final TextView tvOffer = planView.findViewById(R.id.tvOffer);
        final TextView tvAmount = planView.findViewById(R.id.tvAmount);

        tvPlanType.setText(mPlanList.get(position).getPlanType());
        tvDiscount.setText(mPlanList.get(position).getDiscount().toString());
        tvOffer.setText(mPlanList.get(position).getOffer());
        tvAmount.setText(mPlanList.get(position).getPlanAmount().toString());

        btnPurchase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onPurchaseClickListener.onPurchaseClick(mPlanList.get(position).getId());
            }
        });
        return planView;
    }
}
