package com.eshiksa.devicetracking.presenterimpl;

import com.eshiksa.devicetracking.pojo.LoginEntity;
import com.eshiksa.devicetracking.presenter.LoginPresenter;
import com.eshiksa.devicetracking.service.LoginService;
import com.eshiksa.devicetracking.serviceimpl.activity.LoginServiceImpl;
import com.eshiksa.devicetracking.view.LoginView;

/**
 * @author by SHUSHMA on 23/4/18.
 */

public class LoginPresenterImpl implements LoginPresenter {

    private LoginView loginView;

    public LoginPresenterImpl(LoginView loginView) {
        this.loginView = loginView;
    }

    @Override
    public void onPrepareCall() {
        if (loginView != null)
            loginView.showProgress();
    }

    @Override
    public void onLoginCall(String username, String password) {

        LoginService loginService = new LoginServiceImpl(this);
        loginService.onLoginServiceCall(username, password);

    }

    @Override
    public void onLoginSuccess(LoginEntity loginEntity) {
        if (loginView != null) {
            loginView.hideProgress();
            loginView.onLoginSuccess(loginEntity);
        }
    }

    @Override
    public void onLoginFailure(String message) {
        loginView.onFailedMessage(message);
    }

    @Override
    public void onLoginError(int requestCode, String message) {
        if (loginView != null) {
            loginView.hideProgress();
            loginView.onServiceError(message);
        }
    }

    @Override
    public void onSignUpCall(String username, String password, String email, String contact, String address) {
        LoginService loginService = new LoginServiceImpl(this);
        loginService.onSignUpServiceCall(username, password, email, contact, address);
    }

    @Override
    public void onSignUpSuccess(LoginEntity loginEntity) {
        if (loginView != null) {
            loginView.hideProgress();
            loginView.onLoginSuccess(loginEntity);
        }
    }

    @Override
    public void onEmailCall(String email) {
        LoginService loginService = new LoginServiceImpl(this);
        loginService.onEmailServiceCall(email);
    }

    @Override
    public void onEmailSuccess(LoginEntity loginEntity) {
        if (loginView != null) {
            loginView.hideProgress();
            loginView.onLoginSuccess(loginEntity);
        }
    }
}
