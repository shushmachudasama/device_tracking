package com.eshiksa.devicetracking.viewimpl.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.eshiksa.devicetracking.R;
import com.eshiksa.devicetracking.pojo.device.device_list.DeviceList;

import java.util.List;

/**
 * @author by AKHILESH on 10/5/18.
 */
public class DeviceListAdapter extends BaseAdapter {

    Context context;
    LayoutInflater inflter;
    List<DeviceList> mDeviceList;
    public DeviceListAdapter(Context applicationContext, List<DeviceList> logos) {
        this.context = applicationContext;
        this.mDeviceList = logos;
        inflter = (LayoutInflater.from(applicationContext));
    }
    @Override
    public int getCount() {
        return mDeviceList.size();
    }
    @Override
    public Object getItem(int i) {
        return null;
    }
    @Override
    public long getItemId(int i) {
        return 0;
    }
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.device_list_gridview_item, null); // inflate the layout
        TextView icon = (TextView) view.findViewById(R.id.txtTitle); // get the reference of ImageView
        DeviceList entity = mDeviceList.get(i);
        icon.setText(entity.getNickName());
        return view;
    }
}

