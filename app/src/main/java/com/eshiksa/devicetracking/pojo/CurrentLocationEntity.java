
package com.eshiksa.devicetracking.pojo;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CurrentLocationEntity implements Parcelable {

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("lattitude")
    @Expose
    private String lattitude;
    @SerializedName("listOfLocation")
    @Expose
    private Object listOfLocation;
    @SerializedName("status")
    @Expose
    private Boolean status;
    public final static Creator<CurrentLocationEntity> CREATOR = new Creator<CurrentLocationEntity>() {


        @SuppressWarnings({
                "unchecked"
        })
        public CurrentLocationEntity createFromParcel(Parcel in) {
            return new CurrentLocationEntity(in);
        }

        public CurrentLocationEntity[] newArray(int size) {
            return (new CurrentLocationEntity[size]);
        }

    };

    protected CurrentLocationEntity(Parcel in) {
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        this.address = ((String) in.readValue((String.class.getClassLoader())));
        this.longitude = ((String) in.readValue((String.class.getClassLoader())));
        this.lattitude = ((String) in.readValue((String.class.getClassLoader())));
        this.status = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.listOfLocation = ((Object) in.readValue((Object.class.getClassLoader())));
    }

    public CurrentLocationEntity() {
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLattitude() {
        return lattitude;
    }

    public void setLattitude(String lattitude) {
        this.lattitude = lattitude;
    }

    public Object getListOfLocation() {
        return listOfLocation;
    }

    public void setListOfLocation(Object listOfLocation) {
        this.listOfLocation = listOfLocation;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(message);
        dest.writeValue(address);
        dest.writeValue(status);
        dest.writeValue(longitude);
        dest.writeValue(lattitude);
        dest.writeValue(listOfLocation);
    }

    public int describeContents() {
        return 0;
    }

}
