package com.eshiksa.devicetracking.view;

import com.eshiksa.devicetracking.pojo.device.device_list.DeviceListMainEntity;

/**
 * @author by SHUSHMA on 11/5/18.
 */

public interface DeviceView extends CommonView {

    void onDeviceListSuccess(DeviceListMainEntity deviceListMainEntity);

    void onFailedMessage(String message);

    void onServiceError(String status);

}
