package com.eshiksa.devicetracking.viewimpl.activity;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.eshiksa.devicetracking.R;
import com.eshiksa.devicetracking.notification.Config;
import com.eshiksa.devicetracking.notification.NotificationUtils;
import com.eshiksa.devicetracking.pojo.CurrentLocationEntity;
import com.eshiksa.devicetracking.pojo.device.device_list.DeviceList;
import com.eshiksa.devicetracking.pojo.device.device_list.DeviceListMainEntity;
import com.eshiksa.devicetracking.pojo.order.ListOfOrder;
import com.eshiksa.devicetracking.pojo.order.OrderStatusEntity;
import com.eshiksa.devicetracking.presenter.DevicePresenter;
import com.eshiksa.devicetracking.presenterimpl.DevicePresenterImpl;
import com.eshiksa.devicetracking.serviceimpl.ApiClient;
import com.eshiksa.devicetracking.serviceimpl.ApiInterface;
import com.eshiksa.devicetracking.util.Constant;
import com.eshiksa.devicetracking.util.CustomTypefaceSpan;
import com.eshiksa.devicetracking.util.SharePrefrancClass;
import com.eshiksa.devicetracking.util.ShowAlertDialog;
import com.eshiksa.devicetracking.util.Utility;
import com.eshiksa.devicetracking.view.DeviceView;
import com.eshiksa.devicetracking.viewimpl.adapter.GridDeviceAdapter;
import com.eshiksa.devicetracking.viewimpl.fragment.ProgressDialogFragment;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.messaging.FirebaseMessaging;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends AppCompatActivity
        implements LocationListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        OnMapReadyCallback, GoogleMap.OnInfoWindowClickListener,
        View.OnClickListener, DeviceView, GridDeviceAdapter.OnDeviceClickListener {

    private static final String TAG = HomeActivity.class.getSimpleName();
    private static final int REQUEST_CHECK_SETTINGS = 57;
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final long INTERVAL = 1000 * 10; //10 sec
    private ProgressDialogFragment progressDialogFragment;
    private DeviceListMainEntity entity;
    private Dialog deviceDialog;
    private Utility utility;
    private Marker mapMarker;
    private String username;
    private boolean isFirst = true;
    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    Location mCurrentLocation;
    GoogleMap googleMap;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;
    @BindView(R.id.nav_view)
    NavigationView navigationView;
    @BindView(R.id.security_zone)
    LinearLayout securityZone;
    @BindView(R.id.history)
    LinearLayout history;
    @BindView(R.id.locate)
    LinearLayout locate;
    @BindView(R.id.fabDeviceList)
    FloatingActionButton fabDeviceList;

    private BroadcastReceiver mRegistrationBroadcastReceiver;


    @Override
    public void onMapReady(GoogleMap gMap) {
        googleMap = gMap;
        if (googleMap != null) {
            googleMap.getUiSettings().setZoomControlsEnabled(false);
            googleMap.setOnInfoWindowClickListener(this);

        }
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
    }

    // maps integration method
    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(INTERVAL);
        mLocationRequest.setFastestInterval(0);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!isGooglePlayServicesAvailable()) {
            finish();
        }

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);

                    displayFirebaseRegId();

                } else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                    // new push notification is received

                    String message = intent.getStringExtra("message");
                    FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);
                    Log.i("PUSH.......", message);
//                    Toast.makeText(getApplicationContext(), "Push notification: " + message, Toast.LENGTH_LONG).show();

                }
            }
        };

        displayFirebaseRegId();
        createLocationRequest();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        setContentView(R.layout.activity_home);
        SupportMapFragment fm = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        fm.getMapAsync(this);
        utility = new Utility(this);
        deviceDialog = new Dialog(HomeActivity.this);

        ButterKnife.bind(HomeActivity.this);
        setSupportActionBar(toolbar);
        progressDialogFragment = new ProgressDialogFragment();

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        username = SharePrefrancClass.getInstance(getApplicationContext()).getPref(Constant.USERNAME);
        setNavHeader();
        setTypeFace();
        getDeviceList();

    }

    private void displayFirebaseRegId() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        String regId = pref.getString("regId", null);
        Log.e(TAG, "Firebase reg id: " + regId);
        if (!TextUtils.isEmpty(regId)) {
//            txtRegId.setText("Firebase Reg Id: " + regId);
            getNotification(regId);
        } else {
            Toast.makeText(this, "Firebase Reg Id is not recive yet ", Toast.LENGTH_SHORT).show();
        }
    }

    private void getNotification(String regId) {
        ApiInterface apiInterface = ApiClient.getClient(null).create(ApiInterface.class);
        HashMap<String,Object> map = new HashMap<String, Object>();
        map.put("tokenNumber",regId);
        map.put("username",SharePrefrancClass.getInstance(this).getPref(Constant.USERNAME));
        Call<Object> call = apiInterface.registerFirebaseNotification(map);
        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                if (response.body() != null) {
                    Object entity = response.body();


                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {

            }
        });



    }

    /**
     * Method to get list of all devices
     */
    private void getDeviceList() {
        DevicePresenter devicePresenter = new DevicePresenterImpl(this);
        devicePresenter.onDeviceListCall(username);
    }

    @OnClick({R.id.security_zone, R.id.history, R.id.locate, R.id.fabDeviceList, R.id.settings})
    public void onBottomNavClick(View view) {

        Intent intent;
        switch (view.getId()) {

            case R.id.security_zone:
                intent = new Intent(HomeActivity.this, SecurityActivity.class);
                startActivity(intent);
                break;

            case R.id.history:
                intent = new Intent(HomeActivity.this, HistoryActivity.class);
                startActivity(intent);
                break;

            case R.id.locate:
//                intent = new Intent(HomeActivity.this, HistoryActivity.class);
//                startActivity(intent);
                break;

            case R.id.fabDeviceList:
                openDeviceDialog();
                break;

            case R.id.settings:
                intent = new Intent(HomeActivity.this, SettingsActivity.class);
                startActivity(intent);
                break;


        }

    }

    // maps integration
    @Override
    public void onStart() {
        super.onStart();
//        Log.d(TAG, "onStart fired ..............");
        mGoogleApiClient.connect();
    }


    @Override
    public void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();
    }

    private boolean isGooglePlayServicesAvailable() {
        GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();
        int status1 = googleAPI.isGooglePlayServicesAvailable(this);
        if (ConnectionResult.SUCCESS == status1) {
            return true;
        } else {
            googleAPI.getErrorDialog(this, status1,
                    PLAY_SERVICES_RESOLUTION_REQUEST).show();
            return false;
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        startLocationUpdates();
    }

    protected void startLocationUpdates() {
        try {
            PendingResult<com.google.android.gms.common.api.Status> pendingResult =
                    LocationServices.FusedLocationApi.requestLocationUpdates(
                            mGoogleApiClient, mLocationRequest, this);
        } catch (SecurityException se) {
            se.printStackTrace();
        }

        Log.d(TAG, "Location update started ..............: ");
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.d(TAG, "Connection failed: " + connectionResult.toString());
    }

    @Override
    public void onLocationChanged(Location location) {
        mCurrentLocation = location;
        if (mapMarker != null) {
            if (location.getAccuracy() < 50f) {
                mapMarker.remove();
                addMarker();
            }
        } else {
            addMarker();
        }
    }

    private void addMarker() {
        MarkerOptions options = new MarkerOptions();
        options.icon(Constant.bitmapDescriptorFromVector(this, R.drawable.ic_maps));

        LatLng currentLatLng = new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
        options.position(currentLatLng)
                .title("My Location")
                .snippet("Welcome Its you");
        mapMarker = googleMap.addMarker(options);
        if (isFirst) {
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 13));
            isFirst = false;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(mRegistrationBroadcastReceiver);
        stopLocationUpdates();
    }

    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
    }

    @Override
    public void onResume() {
        super.onResume();

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.REGISTRATION_COMPLETE));
        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.PUSH_NOTIFICATION));
        // clear the notification area when the app is opened
        NotificationUtils.clearNotifications(getApplicationContext());

        displayLocationSettingsRequest(this);
    }

    private void displayLocationSettingsRequest(Context context) {
        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API).build();
        googleApiClient.connect();

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(10000 / 2);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final com.google.android.gms.common.api.Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        if (mGoogleApiClient.isConnected()) {
                            startLocationUpdates();
//            Log.d(TAG, "Location update resumed .....................");
                        }
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        Log.i(TAG, "Location settings are not satisfied. Show the user a dialog to upgrade location settings ");

                        try {
                            // Show the dialog by calling startResolutionForResult(), and check the result
                            // in onActivityResult().
                            status.startResolutionForResult(HomeActivity.this, REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            Log.i(TAG, "PendingIntent unable to execute request.");
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        Log.i(TAG, "Location settings are inadequate, and cannot be fixed here. Dialog not created.");
                        break;
                }
            }
        });
    }

    private void setTypeFace() {
        Menu m = navigationView.getMenu();

        for (int i = 0; i < m.size(); i++) {
            MenuItem mi = m.getItem(i);
            //the method we have create in activity
            applyFontToMenuItem(mi);
        }

    }

    private void applyFontToMenuItem(MenuItem mi) {
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/JosefinSans-Regular.ttf");
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new CustomTypefaceSpan("", font), 0, mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    }

    // Nav Header
    private void setNavHeader() {

        View header = navigationView.getHeaderView(0);
        TextView tvHeaderUsername = header.findViewById(R.id.tvHeaderUsername);
        TextView txtOrder = header.findViewById(R.id.txtOrder);
        TextView txtOrderStatus = header.findViewById(R.id.ord_status);
        TextView txtPlan = header.findViewById(R.id.txtPlan);
        TextView txtPersonalInfo = header.findViewById(R.id.txtPersonalInfo);
        TextView txtDeviceList = header.findViewById(R.id.txtDeviceList);
        TextView txtSosCalling = header.findViewById(R.id.txtSosCalling);
        TextView txtLogout = header.findViewById(R.id.txtLogout);

        LinearLayout linOrder = header.findViewById(R.id.linOrder);
        LinearLayout linOrderStatus = header.findViewById(R.id.linOrderStatus);
        LinearLayout linPlan = header.findViewById(R.id.linPlan);
        LinearLayout linPersonalInfo = header.findViewById(R.id.linPersonalInfo);
        LinearLayout linDeviceList = header.findViewById(R.id.linDeviceList);
        LinearLayout linSosCalling = header.findViewById(R.id.linSosCalling);
        LinearLayout linLogout = header.findViewById(R.id.linLogout);
        tvHeaderUsername.setText(username);

        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/JosefinSans-Regular.ttf");
        txtOrder.setTypeface(typeface);
        txtOrderStatus.setTypeface(typeface);
        txtPlan.setTypeface(typeface);
        txtLogout.setTypeface(typeface);
        txtPersonalInfo.setTypeface(typeface);
        txtDeviceList.setTypeface(typeface);
        txtSosCalling.setTypeface(typeface);

        linOrder.setOnClickListener(this);
        linOrderStatus.setOnClickListener(this);
        linPlan.setOnClickListener(this);
        linPersonalInfo.setOnClickListener(this);
        linDeviceList.setOnClickListener(this);
        linSosCalling.setOnClickListener(this);
        linLogout.setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onClick(View v) {
        Intent intent = null;
        switch (v.getId()) {

            case R.id.linOrder:
                intent = new Intent(getApplicationContext(), OrderActivity.class);
                startActivity(intent);
                break;
            case R.id.linOrderStatus:
                viewStatusTask();
                break;
            case R.id.linPlan:
                intent = new Intent(getApplicationContext(), PlanActivity.class);
                startActivity(intent);
                break;
            case R.id.linPersonalInfo:
                intent = new Intent(this, ProfileActivity.class);
                startActivity(intent);
                break;
            case R.id.linDeviceList:
                intent = new Intent(this, DeviceActivity.class);
                startActivity(intent);
                break;
            case R.id.linSosCalling:
                intent = new Intent(this, SOSCallingActivity.class);
                startActivity(intent);
                break;

            case R.id.linLogout:
                SharePrefrancClass.getInstance(this).clearPref(Constant.USERNAME);
                intent = new Intent(this, LoginActivity.class);
                startActivity(intent);
                finish();
                break;
        }

        drawer.closeDrawer(GravityCompat.START);
    }

    /**
     * Method to view order status
     */
    private void viewStatusTask() {

        HashMap<String, Object> statusMap = new HashMap<>();
        statusMap.put(Constant.USERNAME, SharePrefrancClass.getInstance(this).getPref(Constant.USERNAME));
        ApiInterface apiInterface = ApiClient.getClient(null).create(ApiInterface.class);
        Call<OrderStatusEntity> loginEntityCall = apiInterface.checkOrderStatus(statusMap);
        loginEntityCall.enqueue(new Callback<OrderStatusEntity>() {
            @Override
            public void onResponse(Call<OrderStatusEntity> call, Response<OrderStatusEntity> response) {

                if (response.body() != null) {

                    OrderStatusEntity orderStatusEntity = response.body();
                    if (orderStatusEntity.getStatus()) {
                        List<ListOfOrder> mStatusList = orderStatusEntity.getListOfOrders();
                        if (mStatusList != null) {
                            Intent intent = new Intent(HomeActivity.this, OrderStatusActivity.class);
                            intent.putParcelableArrayListExtra("order_status", (ArrayList<? extends Parcelable>) mStatusList);
                            startActivity(intent);
                        }
                    } else {
                        ShowAlertDialog.showAlert(HomeActivity.this, orderStatusEntity.getMessage(), "OK");
                    }

                } else {
                    ShowAlertDialog.showAlert(HomeActivity.this, response.message(), "OK");
                }

            }

            @Override
            public void onFailure(Call<OrderStatusEntity> call, Throwable t) {
                ShowAlertDialog.showAlert(HomeActivity.this, getApplication().getResources().getString(R.string.message_oops), "OK");
            }

        });

    }

    @Override
    public void showProgress() {
        if (progressDialogFragment != null)
            progressDialogFragment.show(getFragmentManager(), "Loading...");
    }

    @Override
    public void hideProgress() {
        if (progressDialogFragment != null)
            progressDialogFragment.dismiss();
    }

    @Override
    public void onDeviceListSuccess(DeviceListMainEntity deviceListMainEntity) {
        fabDeviceList.setVisibility(View.VISIBLE);
        entity = deviceListMainEntity;
        if (entity.getDeviceList().size() > 0) {
            Constant.DEVICE_NUMBER = entity.getDeviceList().get(0).getDeviceNumber();
        }

    }

    private void openDeviceDialog() {

        if (entity.getDeviceList().size() > 0) {
            deviceDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            deviceDialog.setContentView(R.layout.dialog_devices);
            deviceDialog.setCancelable(false);
            deviceDialog.show();

            GridView gridView = deviceDialog.findViewById(R.id.gridView);
            List<DeviceList> mList = entity.getDeviceList();
            GridDeviceAdapter mAdapter = new GridDeviceAdapter(this, mList, this);
            gridView.setAdapter(mAdapter);

            ImageView cancel = deviceDialog.findViewById(R.id.cancel);

            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    deviceDialog.dismiss();
                }
            });

        } else {
            Toast.makeText(this, "No devices available.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onFailedMessage(String message) {
        ShowAlertDialog.showAlert(HomeActivity.this, message, "OK");
        hideProgress();
    }

    @Override
    public void onServiceError(String status) {
        ShowAlertDialog.showAlert(HomeActivity.this, getApplication().getResources().getString(R.string.message_oops), "OK");
        hideProgress();
    }

    @Override
    public void onDeviceClick(String deviceNumber, final String nickname) {
        Constant.DEVICE_NUMBER = deviceNumber;
        deviceDialog.dismiss();
        ApiInterface apiInterface = ApiClient.getClient(null).create(ApiInterface.class);

        HashMap<String, Object> map = new HashMap<>();
        map.put("deviceNumber", deviceNumber);
        Call<CurrentLocationEntity> deviceListEntityCall = apiInterface.getCurrentLocation(map);
        deviceListEntityCall.enqueue(new Callback<CurrentLocationEntity>() {
            @Override
            public void onResponse(Call<CurrentLocationEntity> call, Response<CurrentLocationEntity> response) {

                if (response.body() != null) {

                    CurrentLocationEntity currentLocationEntity = response.body();

                    if (currentLocationEntity.getStatus()) {
                        MarkerOptions options = new MarkerOptions();
                        options.icon(Constant.bitmapDescriptorFromVector(HomeActivity.this, R.drawable.ic_maps));
                        LatLng currentLatLng = new LatLng(Double.parseDouble(currentLocationEntity.getLattitude()), Double.parseDouble(currentLocationEntity.getLongitude()));
                        options.position(currentLatLng).title(nickname);
                        mapMarker = googleMap.addMarker(options);
                        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 15));


                    } else {
                        Toast.makeText(HomeActivity.this, "Unable to get current location.", Toast.LENGTH_SHORT).show();
                    }

                }

            }

            @Override
            public void onFailure(Call<CurrentLocationEntity> call, Throwable t) {
//                devicePresenter.onDeviceListError(HttpURLConnection.HTTP_INTERNAL_ERROR, "Oops! Some problem seems to have happened. Please try again");
            }

        });
    }

}
