package com.eshiksa.devicetracking.view;

import com.eshiksa.devicetracking.pojo.safezone.SafeZoneEntity;

/**
 * @author by SHUSHMA on 12/5/18.
 */

public interface SafezoneView extends CommonView {

    void onSafeZoneSuccess(SafeZoneEntity safeZoneEntity);

    void onSafeZoneListSuccess(SafeZoneEntity safeZoneEntity);

    void onFailedMessage(String message);

    void onServiceError(String message);
}
