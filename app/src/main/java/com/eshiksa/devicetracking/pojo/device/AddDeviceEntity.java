package com.eshiksa.devicetracking.pojo.device;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author by AKHILESH on 9/5/18.
 */
public class AddDeviceEntity implements Parcelable
{

    @SerializedName("messages")
    @Expose
    private String messages;
    @SerializedName("status")
    @Expose
    private String status;
    public final static Parcelable.Creator<AddDeviceEntity> CREATOR = new Creator<AddDeviceEntity>() {


        @SuppressWarnings({
                "unchecked"
        })
        public AddDeviceEntity createFromParcel(Parcel in) {
            return new AddDeviceEntity(in);
        }

        public AddDeviceEntity[] newArray(int size) {
            return (new AddDeviceEntity[size]);
        }

    }
            ;

    protected AddDeviceEntity(Parcel in) {
        this.messages = ((String) in.readValue((String.class.getClassLoader())));
        this.status = ((String) in.readValue((String.class.getClassLoader())));
    }

    public AddDeviceEntity() {
    }

    public String getMessages() {
        return messages;
    }

    public void setMessages(String messages) {
        this.messages = messages;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(messages);
        dest.writeValue(status);
    }

    public int describeContents() {
        return 0;
    }

}