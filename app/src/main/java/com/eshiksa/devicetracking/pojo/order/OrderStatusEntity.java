
package com.eshiksa.devicetracking.pojo.order;

import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderStatusEntity implements Parcelable {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("listOfOrders")
    @Expose
    private List<ListOfOrder> listOfOrders = null;
    public final static Creator<OrderStatusEntity> CREATOR = new Creator<OrderStatusEntity>() {


        @SuppressWarnings({
                "unchecked"
        })
        public OrderStatusEntity createFromParcel(Parcel in) {
            return new OrderStatusEntity(in);
        }

        public OrderStatusEntity[] newArray(int size) {
            return (new OrderStatusEntity[size]);
        }

    };

    protected OrderStatusEntity(Parcel in) {
        this.status = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        in.readList(this.listOfOrders, (ListOfOrder.class.getClassLoader()));
    }

    public OrderStatusEntity() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<ListOfOrder> getListOfOrders() {
        return listOfOrders;
    }

    public void setListOfOrders(List<ListOfOrder> listOfOrders) {
        this.listOfOrders = listOfOrders;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(status);
        dest.writeValue(message);
        dest.writeList(listOfOrders);
    }

    public int describeContents() {
        return 0;
    }

}
