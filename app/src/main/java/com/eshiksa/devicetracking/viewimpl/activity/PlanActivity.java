package com.eshiksa.devicetracking.viewimpl.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.eshiksa.devicetracking.R;
import com.eshiksa.devicetracking.pojo.paymentpage.PaymentResponseEntity;
import com.eshiksa.devicetracking.pojo.plan.DevicePlansVO;
import com.eshiksa.devicetracking.pojo.plan.PlanEntity;
import com.eshiksa.devicetracking.presenter.PlanPresenter;
import com.eshiksa.devicetracking.presenterimpl.PlanPresenterImpl;
import com.eshiksa.devicetracking.serviceimpl.ApiClient;
import com.eshiksa.devicetracking.serviceimpl.ApiInterface;
import com.eshiksa.devicetracking.util.Constant;
import com.eshiksa.devicetracking.util.SharePrefrancClass;
import com.eshiksa.devicetracking.util.ShowAlertDialog;
import com.eshiksa.devicetracking.view.PlanView;
import com.eshiksa.devicetracking.viewimpl.adapter.PlanAdapter;
import com.eshiksa.devicetracking.viewimpl.fragment.ProgressDialogFragment;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PlanActivity extends AppCompatActivity implements PlanView, PlanAdapter.OnPurchaseClickListener{
    @BindView(R.id.planGrid)
    GridView planGrid;

    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.imageBack)
    ImageView imageBack;

    private ProgressDialogFragment progressDialogFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plan);
        ButterKnife.bind(this);
        progressDialogFragment = new ProgressDialogFragment();
        setTypeFaceLayout();
        getPlan();

    }

    @OnClick({
            R.id.imageBack})
    public void placeOrder(View view) {

        switch (view.getId()) {
            case R.id.imageBack:
                onBackPressed();
                break;
        }
    }

    private void setTypeFaceLayout() {
        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/JosefinSans-Regular.ttf");
        txtTitle.setTypeface(typeface);
    }

    /**
     * Method to get list of plans
     */
    private void getPlan() {
        PlanPresenter planPresenter = new PlanPresenterImpl(this);
        planPresenter.onPlanCall();
    }

    @Override
    public void showProgress() {
        if (progressDialogFragment != null) {
            progressDialogFragment.show(this.getFragmentManager(), getResources().getString(R.string.msg_progress_dialog));
        }
    }

    @Override
    public void hideProgress() {
        if (progressDialogFragment != null) {
            progressDialogFragment.dismiss();
        }
    }

    @Override
    public void onPlanSuccess(PlanEntity planEntity) {

        List<DevicePlansVO> devicePlansVOS = planEntity.getDevicePlansVOs();
        if (devicePlansVOS.size() > 0) {

            PlanAdapter planAdapter = new PlanAdapter(this, devicePlansVOS, this);
            planGrid.setAdapter(planAdapter);

        } else {
            Toast.makeText(this, "No plans found.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onFailedMessage(String message) {
        ShowAlertDialog.showAlert(this, message, "OK");
        hideProgress();
    }

    @Override
    public void onServiceError(String message) {
        ShowAlertDialog.showAlert(this, message, "OK");
        hideProgress();
    }

    @Override
    public void onPurchaseClick(Integer id) {
        onPlanClick(id);
    }

    private void onPlanClick(Integer id) {

        showProgress();
        Long idd = new Long(id);
        ApiInterface apiInterface = ApiClient.getClient(null).create(ApiInterface.class);
        String username = SharePrefrancClass.getInstance(this).getPref(Constant.USERNAME);
        HashMap<String,Object> params = new HashMap<String, Object>();
        params.put("planId",idd);
        params.put("userName",username);
        params.put("deviceNumber",Constant.DEVICE_NUMBER);
        params.put("orderId",SharePrefrancClass.getInstance(PlanActivity.this).getPref(Constant.ORDERID));
        Call<PaymentResponseEntity> call = apiInterface.gotoPaymentPage(params);
        call.enqueue(new Callback<PaymentResponseEntity>() {
            @Override
            public void onResponse(Call<PaymentResponseEntity> call, Response<PaymentResponseEntity> response) {

                if (response.body()!=null) {
                    hideProgress();
                    PaymentResponseEntity entity = response.body();

                    if (entity.getStatus()){
                        String url = entity.getRedirectUrl();
                        if (url!=null) {

                            Intent intent = new Intent(PlanActivity.this, WebViewActivity.class);
                            intent.putExtra("URL", url);
                            startActivity(intent);
                        }

                    } else {
                        ShowAlertDialog.showAlert(PlanActivity.this,entity.getMessage(),"OK");
                    }

                }
            }

            @Override
            public void onFailure(Call<PaymentResponseEntity> call, Throwable t) {
                ShowAlertDialog.showAlert(PlanActivity.this,getApplication().getResources().getString(R.string.message_oops),"OK");

            }
        });
    }
}
