package com.eshiksa.devicetracking.viewimpl;

import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;

import com.eshiksa.devicetracking.R;
import com.eshiksa.devicetracking.util.Utility;

/**
 * @author by AKHILESH on 4/5/18.
 */
public class PermissionActivity extends AppCompatActivity {

    private static final int PERMISSION_ALL = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_permission);
        if (getIntent().getStringExtra("TYPE").equals("1")) {
            String[] PERMISSIONS = {android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION};
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        } else if (getIntent().getStringExtra("TYPE").equals("2")) {
            new Utility(this).openGpsSetting();
        }
    }
}
