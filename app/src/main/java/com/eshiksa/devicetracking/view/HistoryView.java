package com.eshiksa.devicetracking.view;

import com.eshiksa.devicetracking.pojo.history.HistoryEntity;

/**
 * @author by SHUSHMA on 16/5/18.
 */

public interface HistoryView extends CommonView {

    void onHistorySuccess(HistoryEntity historyEntity);

    void onFailedMessage(String message);

    void onServiceError(String message);

}
