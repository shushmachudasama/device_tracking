package com.eshiksa.devicetracking.presenterimpl;

import com.eshiksa.devicetracking.pojo.history.HistoryEntity;
import com.eshiksa.devicetracking.presenter.HistoryPresenter;
import com.eshiksa.devicetracking.service.HistoryService;
import com.eshiksa.devicetracking.serviceimpl.activity.HistoryServiceImpl;
import com.eshiksa.devicetracking.view.HistoryView;

import java.util.HashMap;

/**
 * @author by SHUSHMA on 16/5/18.
 */

public class HistoryPresenterImpl implements HistoryPresenter {
    
    private HistoryView historyView;

    public HistoryPresenterImpl(HistoryView historyView) {
        this.historyView = historyView;
    }

    @Override
    public void onPrepareCall() {
        if (historyView != null)
            historyView.showProgress();
    }

    @Override
    public void onHistoryCall(HashMap<String, Object> map) {
        HistoryService historyService = new HistoryServiceImpl(this);
        historyService.historyCall(map);
    }

    @Override
    public void onHistorySuccess(HistoryEntity historyEntity) {
        if (historyView != null) {
            historyView.hideProgress();
            historyView.onHistorySuccess(historyEntity);
        }
    }

    @Override
    public void onHistoryFailure(String message) {
        historyView.onFailedMessage(message);
    }

    @Override
    public void onHistoryError(int requestCode, String message) {
        if (historyView != null) {
            historyView.hideProgress();
            historyView.onServiceError(message);
        }
    }
}
