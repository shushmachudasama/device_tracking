package com.eshiksa.devicetracking.viewimpl.activity;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.eshiksa.devicetracking.R;
import com.eshiksa.devicetracking.pojo.device.device_list.DeviceList;
import com.eshiksa.devicetracking.pojo.device.device_list.DeviceListMainEntity;
import com.eshiksa.devicetracking.presenter.DevicePresenter;
import com.eshiksa.devicetracking.presenterimpl.DevicePresenterImpl;
import com.eshiksa.devicetracking.util.Constant;
import com.eshiksa.devicetracking.util.SharePrefrancClass;
import com.eshiksa.devicetracking.view.DeviceView;
import com.eshiksa.devicetracking.viewimpl.adapter.SOSAdapter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SOSCallingActivity extends AppCompatActivity implements
        SOSAdapter.OnSosCallClickListner, DeviceView {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.imageBack)
    ImageView imgBack;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.fab)
    FloatingActionButton fab;
    private List<DeviceList> mList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_soscalling);
        ButterKnife.bind(this);
        setTypeFaceLayout();

        recyclerView.setLayoutManager(new LinearLayoutManager(this));


    }

    @Override
    protected void onResume() {
        super.onResume();
        getDeviceList();
    }

    private void getDeviceList() {
        String username = SharePrefrancClass.getInstance(this).getPref(Constant.USERNAME);
        DevicePresenter devicePresenter = new DevicePresenterImpl(this);
        devicePresenter.onDeviceListCall(username);
    }


    private void setTypeFaceLayout() {
        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/JosefinSans-Regular.ttf");
        txtTitle.setTypeface(typeface);

    }

    @Override
    public void onDeviceClick(String deviceNumber) {

    }

    @OnClick({R.id.imageBack, R.id.fab})
    void onClickBtn(View view) {
        switch (view.getId()) {
            case R.id.imageBack:
                onBackPressed();
                break;
            case R.id.fab:
                openDialogBox();
                break;
            default:
                break;
        }
    }

    Dialog dialog;

    private void openDialogBox() {
        dialog = new Dialog(SOSCallingActivity.this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.add_sos_device);
        dialog.setCancelable(false);
        dialog.show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onDeviceListSuccess(DeviceListMainEntity deviceListMainEntity) {
        mList = deviceListMainEntity.getDeviceList();

        if (mList.size() > 0) {
            SOSAdapter mSosAdapter = new SOSAdapter(this, this, mList);
//        mSosAdapter.setSosAdaterListner(SOSCallingActivity.this);
            recyclerView.setAdapter(mSosAdapter);


          /*  DeviceAdapter deviceAdapter = new DeviceAdapter(this, mList);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(DeviceActivity.this);
            recyclerDevices.setLayoutManager(layoutManager);
            recyclerDevices.setAdapter(deviceAdapter);*/
        }
    }

    @Override
    public void onFailedMessage(String message) {

    }

    @Override
    public void onServiceError(String status) {

    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }
}
