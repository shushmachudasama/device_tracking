package com.eshiksa.devicetracking.view;

import com.eshiksa.devicetracking.pojo.safezone.SafeZoneEntity;

/**
 * @author by SHUSHMA on 16/5/18.
 */

public interface ModifyZoneView extends CommonView {

    void onModifyZoneSuccess(SafeZoneEntity safeZoneEntity);

    void onDeleteZoneSuccess(SafeZoneEntity safeZoneEntity);

    void onFailedMessage(String message);

    void onServiceError(String message);

}
