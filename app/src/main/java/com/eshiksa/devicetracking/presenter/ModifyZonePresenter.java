package com.eshiksa.devicetracking.presenter;

import com.eshiksa.devicetracking.pojo.safezone.SafeZoneEntity;

/**
 * @author by SHUSHMA on 16/5/18.
 */

public interface ModifyZonePresenter {

    void onPrepareCall();

    void onModifyZoneCall(SafeZoneEntity safeZoneEntity);

    void onModifyZoneSuccess(SafeZoneEntity safeZoneEntity);

    void onDeleteZoneCall(String zoneId);

    void onDeleteZoneSuccess(SafeZoneEntity safeZoneEntity);

    void onModifyZoneFailure(String message);

    void onModifyZoneError(int requestCode, String message);

}
