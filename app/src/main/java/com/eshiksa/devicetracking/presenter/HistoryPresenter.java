package com.eshiksa.devicetracking.presenter;

import com.eshiksa.devicetracking.pojo.history.HistoryEntity;

import java.util.HashMap;

/**
 * @author by SHUSHMA on 16/5/18.
 */

public interface HistoryPresenter {

    void onPrepareCall();

    void onHistoryCall(HashMap<String, Object> map);

    void onHistorySuccess(HistoryEntity historyEntity);

    void onHistoryFailure(String message);

    void onHistoryError(int requestCode, String message);
}
