package com.eshiksa.devicetracking.view;

/**
 * @author by SHUSHMA on 23/4/18.
 */

public interface CommonView {

    void showProgress();

    void hideProgress();

}
