package com.eshiksa.devicetracking.serviceimpl.activity;

import com.eshiksa.devicetracking.pojo.history.HistoryEntity;
import com.eshiksa.devicetracking.pojo.safezone.SafeZoneEntity;
import com.eshiksa.devicetracking.presenter.HistoryPresenter;
import com.eshiksa.devicetracking.service.HistoryService;
import com.eshiksa.devicetracking.serviceimpl.ApiClient;
import com.eshiksa.devicetracking.serviceimpl.ApiInterface;

import java.net.HttpURLConnection;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * @author by SHUSHMA on 16/5/18.
 */

public class HistoryServiceImpl implements HistoryService {

    private HistoryPresenter historyPresenter;

    public HistoryServiceImpl(HistoryPresenter historyPresenter) {
        this.historyPresenter = historyPresenter;
    }

    @Override
    public void historyCall(HashMap<String, Object> map) {
        historyPresenter.onPrepareCall();

        ApiInterface apiInterface = ApiClient.getClient(null).create(ApiInterface.class);
        Call<HistoryEntity> loginEntityCall = apiInterface.getHistory(map);
        loginEntityCall.enqueue(new Callback<HistoryEntity>() {
            @Override
            public void onResponse(Call<HistoryEntity> call, Response<HistoryEntity> response) {

                if (response.body() != null) {

                    HistoryEntity historyEntity = response.body();

                    if (historyEntity.getStatus()) {
                        historyPresenter.onHistorySuccess(historyEntity);
                    } else {
                        historyPresenter.onHistoryFailure(historyEntity.getMessage());
                    }

                } else {
                    historyPresenter.onHistoryFailure(response.message());
                }

            }

            @Override
            public void onFailure(Call<HistoryEntity> call, Throwable t) {
                historyPresenter.onHistoryError(HttpURLConnection.HTTP_INTERNAL_ERROR, "Oops! Some problem seems to have happened. Please try again");
            }
        });
    }
}
