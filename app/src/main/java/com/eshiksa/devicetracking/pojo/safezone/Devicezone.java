
package com.eshiksa.devicetracking.pojo.safezone;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Devicezone implements Parcelable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("userId")
    @Expose
    private Integer userId;
    @SerializedName("deviceNumber")
    @Expose
    private String deviceNumber;
    @SerializedName("latitude")
    @Expose
    private Double latitude;
    @SerializedName("longitude")
    @Expose
    private Double longitude;
    @SerializedName("distance")
    @Expose
    private Integer distance;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("placeName")
    @Expose
    private String placeName;
    @SerializedName("alternatePlaceName")
    @Expose
    private String alternatePlaceName;
    public final static Creator<Devicezone> CREATOR = new Creator<Devicezone>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Devicezone createFromParcel(Parcel in) {
            return new Devicezone(in);
        }

        public Devicezone[] newArray(int size) {
            return (new Devicezone[size]);
        }

    };

    protected Devicezone(Parcel in) {
        this.id = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.userId = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.deviceNumber = ((String) in.readValue((String.class.getClassLoader())));
        this.latitude = ((Double) in.readValue((Double.class.getClassLoader())));
        this.longitude = ((Double) in.readValue((Double.class.getClassLoader())));
        this.distance = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.username = ((String) in.readValue((String.class.getClassLoader())));
        this.placeName = ((String) in.readValue((String.class.getClassLoader())));
        this.alternatePlaceName = ((String) in.readValue((String.class.getClassLoader())));
    }

    public Devicezone() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getDeviceNumber() {
        return deviceNumber;
    }

    public void setDeviceNumber(String deviceNumber) {
        this.deviceNumber = deviceNumber;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Integer getDistance() {
        return distance;
    }

    public void setDistance(Integer distance) {
        this.distance = distance;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPlaceName() {
        return placeName;
    }

    public void setPlaceName(String placeName) {
        this.placeName = placeName;
    }

    public String getAlternatePlaceName() {
        return alternatePlaceName;
    }

    public void setAlternatePlaceName(String alternatePlaceName) {
        this.alternatePlaceName = alternatePlaceName;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(userId);
        dest.writeValue(deviceNumber);
        dest.writeValue(latitude);
        dest.writeValue(longitude);
        dest.writeValue(distance);
        dest.writeValue(username);
        dest.writeValue(placeName);
        dest.writeValue(alternatePlaceName);
    }

    public int describeContents() {
        return 0;
    }

}
