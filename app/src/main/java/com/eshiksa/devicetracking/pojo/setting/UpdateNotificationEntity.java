package com.eshiksa.devicetracking.pojo.setting;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author by AKHILESH on 29/5/18.
 */
public class UpdateNotificationEntity implements Parcelable {

    @SerializedName("message")
    @Expose
    private String messages;
    @SerializedName("status")
    @Expose
    private Boolean status;
    public final static Parcelable.Creator<UpdateNotificationEntity> CREATOR = new Creator<UpdateNotificationEntity>() {


        @SuppressWarnings({
                "unchecked"
        })
        public UpdateNotificationEntity createFromParcel(Parcel in) {
            return new UpdateNotificationEntity(in);
        }

        public UpdateNotificationEntity[] newArray(int size) {
            return (new UpdateNotificationEntity[size]);
        }

    }
            ;

    protected UpdateNotificationEntity(Parcel in) {
        this.messages = ((String) in.readValue((String.class.getClassLoader())));
        this.status = ((Boolean) in.readValue((String.class.getClassLoader())));
    }

    public UpdateNotificationEntity() {
    }

    public String getMessages() {
        return messages;
    }

    public void setMessages(String messages) {
        this.messages = messages;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(messages);
        dest.writeValue(status);
    }

    public int describeContents() {
        return 0;
    }
}
