package com.eshiksa.devicetracking.viewimpl.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.CompoundButton;
import android.widget.Switch;

import com.eshiksa.devicetracking.R;
import com.eshiksa.devicetracking.pojo.setting.CheckSwitchEntity;
import com.eshiksa.devicetracking.pojo.setting.UpdateNotificationEntity;
import com.eshiksa.devicetracking.serviceimpl.ApiClient;
import com.eshiksa.devicetracking.serviceimpl.ApiInterface;
import com.eshiksa.devicetracking.util.Constant;
import com.eshiksa.devicetracking.util.SharePrefrancClass;
import com.eshiksa.devicetracking.util.ShowAlertDialog;
import com.google.gson.Gson;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SettingsActivity extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener {

    @BindView(R.id.notificationSwitch)
    Switch notificationSwitch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        ButterKnife.bind(SettingsActivity.this);

        callCheckSwitchEnable();

        notificationSwitch.setOnCheckedChangeListener(this);


    }

    private void callCheckSwitchEnable() {
        HashMap<String, Object> hashMap = new HashMap<String, Object>();
//        hashMap.put("deviceNumber", Constant.DEVICE_NUMBER);
        hashMap.put("username", SharePrefrancClass.getInstance(this).getPref(Constant.USERNAME));
        ApiInterface apiInterface = ApiClient.getClient(null).create(ApiInterface.class);
        Call<CheckSwitchEntity> entityCall = apiInterface.checkswitchEnabled(hashMap);
        entityCall.enqueue(new Callback<CheckSwitchEntity>() {
            @Override
            public void onResponse(Call<CheckSwitchEntity> call, Response<CheckSwitchEntity> response) {
                if (response.body() != null) {
                    CheckSwitchEntity entity = response.body();
                    if (entity.getStatus()) {
                        Log.d("TAGGG", new Gson().toJson(entity));

                        if (entity.getIsEnabled()) {
                            notificationSwitch.setChecked(true);
                        } else {
                            notificationSwitch.setChecked(false);
                        }

                    } else {
                        Log.d("TAGGG", new Gson().toJson(entity));

                    }
                }
            }

            @Override
            public void onFailure(Call<CheckSwitchEntity> call, Throwable t) {

            }
        });
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            callUpdateNotification(isChecked);

        } else {
            callUpdateNotification(isChecked);
        }


    }

    private void callUpdateNotification(final boolean isChecked) {
        HashMap<String, Object> hashMap = new HashMap<String, Object>();
//        hashMap.put("deviceNumber", Constant.DEVICE_NUMBER);
        hashMap.put("username", SharePrefrancClass.getInstance(this).getPref(Constant.USERNAME));
        ApiInterface apiInterface = ApiClient.getClient(null).create(ApiInterface.class);
        Call<UpdateNotificationEntity> call = apiInterface.updateNotification(hashMap);
        call.enqueue(new Callback<UpdateNotificationEntity>() {
            @Override
            public void onResponse(Call<UpdateNotificationEntity> call, Response<UpdateNotificationEntity> response) {
                if (response.body() != null) {
                    UpdateNotificationEntity entity = response.body();
                    if (entity.getStatus()) {
                        ShowAlertDialog.showAlert(SettingsActivity.this, entity.getMessages(), "OK");
                        if (entity.getMessages().equalsIgnoreCase("Successfully Disabled ")) {
                            notificationSwitch.setChecked(isChecked);
                        } else {
                            notificationSwitch.setChecked(isChecked);
                        }
                    } else {
                        ShowAlertDialog.showAlert(SettingsActivity.this, entity.getMessages(), "OK");

                    }
                }
            }

            @Override
            public void onFailure(Call<UpdateNotificationEntity> call, Throwable t) {

            }
        });
    }
}
