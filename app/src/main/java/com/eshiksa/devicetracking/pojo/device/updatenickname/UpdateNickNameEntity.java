package com.eshiksa.devicetracking.pojo.device.updatenickname;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author by AKHILESH on 15/5/18.
 */
public class UpdateNickNameEntity implements Parcelable{

    @SerializedName("messages")
    @Expose
    private String messages;
    @SerializedName("status")
    @Expose
    private Boolean status;
    public final static Parcelable.Creator<UpdateNickNameEntity> CREATOR = new Creator<UpdateNickNameEntity>() {


        @SuppressWarnings({
                "unchecked"
        })
        public UpdateNickNameEntity createFromParcel(Parcel in) {
            return new UpdateNickNameEntity(in);
        }

        public UpdateNickNameEntity[] newArray(int size) {
            return (new UpdateNickNameEntity[size]);
        }

    }
            ;

    protected UpdateNickNameEntity(Parcel in) {
        this.messages = ((String) in.readValue((String.class.getClassLoader())));
        this.status = ((Boolean) in.readValue((String.class.getClassLoader())));
    }

    public UpdateNickNameEntity() {
    }

    public String getMessages() {
        return messages;
    }

    public void setMessages(String messages) {
        this.messages = messages;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(messages);
        dest.writeValue(status);
    }

    public int describeContents() {
        return 0;
    }
}
