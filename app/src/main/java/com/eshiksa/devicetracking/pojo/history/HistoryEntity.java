
package com.eshiksa.devicetracking.pojo.history;

import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HistoryEntity implements Parcelable {

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("listOfLocation")
    @Expose
    private List<ListOfLocation> listOfLocation = null;
    public final static Creator<HistoryEntity> CREATOR = new Creator<HistoryEntity>() {


        @SuppressWarnings({
                "unchecked"
        })
        public HistoryEntity createFromParcel(Parcel in) {
            return new HistoryEntity(in);
        }

        public HistoryEntity[] newArray(int size) {
            return (new HistoryEntity[size]);
        }

    };

    protected HistoryEntity(Parcel in) {
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        this.status = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        in.readList(this.listOfLocation, (ListOfLocation.class.getClassLoader()));
    }

    public HistoryEntity() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<ListOfLocation> getListOfLocation() {
        return listOfLocation;
    }

    public void setListOfLocation(List<ListOfLocation> listOfLocation) {
        this.listOfLocation = listOfLocation;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(message);
        dest.writeValue(status);
        dest.writeList(listOfLocation);
    }

    public int describeContents() {
        return 0;
    }

}
