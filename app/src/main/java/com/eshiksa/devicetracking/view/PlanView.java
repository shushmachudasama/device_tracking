package com.eshiksa.devicetracking.view;

import com.eshiksa.devicetracking.pojo.plan.PlanEntity;

/**
 * @author by SHUSHMA on 24/4/18.
 */

public interface PlanView extends CommonView {

    void onPlanSuccess(PlanEntity planEntity);

    void onFailedMessage(String message);

    void onServiceError(String status);

}
