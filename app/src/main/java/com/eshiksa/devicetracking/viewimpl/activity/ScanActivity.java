package com.eshiksa.devicetracking.viewimpl.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.eshiksa.devicetracking.R;
import com.eshiksa.devicetracking.pojo.ScanData;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ScanActivity extends AppCompatActivity {
    @BindView(R.id.imageScan)
    ImageView imageScan;
    @BindView(R.id.txtScan)
    TextView txtScan;
    private final int BAR_CODE_REQUEST = 101;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.imageScan})
    public void onScanClick(View view) {
        switch (view.getId()) {
            case R.id.imageScan:
                Intent scanIntent = new Intent(this, ScannerActivity.class);
                startActivityForResult(scanIntent, BAR_CODE_REQUEST);
                break;
            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {

            case BAR_CODE_REQUEST:
                if (resultCode == RESULT_OK) {
                    String scanData = data.getStringExtra("SCAN_DATA");
                    txtScan.setText(scanData);

                }
                break;
        }
    }
}
