package com.eshiksa.devicetracking.serviceimpl;

import android.support.annotation.NonNull;

import com.eshiksa.devicetracking.BuildConfig;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * @author by SHUSHMA on 23/4/18.
 */

public class ApiClient {

//    public static String BASE_URL = "http://115.124.100.177:8080/tracking/";
//    public static String BASE_URL = "http://192.168.1.218:8082/tracking/";
    public static String BASE_URL = "http://35.200.153.165:8080/TrackingApp/";

    static Retrofit retrofit = null;

    public static Retrofit getClient(Map<String, String> headerMap) {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit.Builder builder = new Retrofit.Builder();
        builder.baseUrl(BASE_URL);
        builder.addConverterFactory(GsonConverterFactory.create(gson));

        if (headerMap != null) {
            builder.client(getHeader(headerMap));
        } else {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor()
                    .setLevel(HttpLoggingInterceptor.Level.BODY);

            OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
            httpClient.readTimeout(60, TimeUnit.SECONDS);
            httpClient.connectTimeout(60, TimeUnit.SECONDS);
            if (BuildConfig.DEBUG)
                httpClient.addInterceptor(interceptor);
            builder.client(httpClient.build());
        }
        retrofit = builder.build();
        return retrofit;
    }

    @NonNull
    private static OkHttpClient getHeader(final Map<String, String> header) {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor()
                .setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Interceptor.Chain chain) throws IOException {
                Request original = chain.request();

                Request.Builder builder = original.newBuilder();
                for (Map.Entry<String, String> entry : header.entrySet()) {
                    builder.addHeader(entry.getKey(), entry.getValue());
                }
                builder.method(original.method(), original.body());
                return chain.proceed(builder.build());
            }
        });
        if (BuildConfig.DEBUG) //hides exposing web service call in logcat when app is released
            httpClient.addInterceptor(interceptor);
        return httpClient.build();

    }
}
