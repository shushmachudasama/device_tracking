package com.eshiksa.devicetracking.viewimpl.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.eshiksa.devicetracking.R;
import com.eshiksa.devicetracking.pojo.order.ListOfOrder;
import com.eshiksa.devicetracking.viewimpl.adapter.OrderStatusAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OrderStatusActivity extends AppCompatActivity {

    @BindView(R.id.recyclerOrderStatus)
    RecyclerView recyclerOrderStatus;
    @BindView(R.id.imageBack)
    ImageView imageBack;

    private List<ListOfOrder> mOrdStatusList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_status);
        ButterKnife.bind(OrderStatusActivity.this);

        mOrdStatusList = new ArrayList<>();

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            mOrdStatusList = bundle.getParcelableArrayList("order_status");

            if (mOrdStatusList.size() > 0) {

                OrderStatusAdapter orderStatusAdapter = new OrderStatusAdapter(mOrdStatusList, OrderStatusActivity.this);
                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(OrderStatusActivity.this);
                recyclerOrderStatus.setLayoutManager(layoutManager);
                recyclerOrderStatus.setAdapter(orderStatusAdapter);

            }

        }

    }

    @OnClick({R.id.imageBack})
    public void onClickBtn(View view) {

        switch (view.getId()) {

            case R.id.imageBack:
                onBackPressed();
                break;
        }
    }
}
