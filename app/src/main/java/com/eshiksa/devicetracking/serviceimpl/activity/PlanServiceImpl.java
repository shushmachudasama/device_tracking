package com.eshiksa.devicetracking.serviceimpl.activity;

import com.eshiksa.devicetracking.pojo.plan.PlanEntity;
import com.eshiksa.devicetracking.presenter.PlanPresenter;
import com.eshiksa.devicetracking.service.PlanService;
import com.eshiksa.devicetracking.serviceimpl.ApiClient;
import com.eshiksa.devicetracking.serviceimpl.ApiInterface;

import java.net.HttpURLConnection;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * @author by SHUSHMA on 24/4/18.
 */

public class PlanServiceImpl implements PlanService {

    private PlanPresenter planPresenter;

    public PlanServiceImpl(PlanPresenter planPresenter) {
        this.planPresenter = planPresenter;
    }

    @Override
    public void planCall() {
        planPresenter.onPrepareCall();

        ApiInterface apiInterface = ApiClient.getClient(null).create(ApiInterface.class);
        Call<PlanEntity> loginEntityCall = apiInterface.getPlans();
        loginEntityCall.enqueue(new Callback<PlanEntity>() {
            @Override
            public void onResponse(Call<PlanEntity> call, Response<PlanEntity> response) {

                if (response.body() != null) {

                    PlanEntity planEntity = response.body();
                    if (planEntity.getStatus()) {
                        planPresenter.onPlanSuccess(planEntity);
                    } else {
                        planPresenter.onPlanFailure(planEntity.getMessage());
                    }

                } else {
                    planPresenter.onPlanFailure(response.message());
                }

            }

            @Override
            public void onFailure(Call<PlanEntity> call, Throwable t) {
                planPresenter.onPlanError(HttpURLConnection.HTTP_INTERNAL_ERROR, "Oops! Some problem seems to have happened. Please try again");
            }

        });

    }
}
