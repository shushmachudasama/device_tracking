package com.eshiksa.devicetracking.serviceimpl.activity;

import com.eshiksa.devicetracking.pojo.order.OrderEntity;
import com.eshiksa.devicetracking.presenter.OrderPresenter;
import com.eshiksa.devicetracking.service.OrderService;
import com.eshiksa.devicetracking.serviceimpl.ApiClient;
import com.eshiksa.devicetracking.serviceimpl.ApiInterface;

import java.net.HttpURLConnection;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * @author by SHUSHMA on 24/4/18.
 */

public class OrderServiceImpl implements OrderService {

    private OrderPresenter orderPresenter;

    public OrderServiceImpl(OrderPresenter orderPresenter) {
        this.orderPresenter = orderPresenter;
    }

    @Override
    public void orderCall(OrderEntity orderEntity) {
        orderPresenter.onPrepareCall();

        ApiInterface apiInterface = ApiClient.getClient(null).create(ApiInterface.class);
        Call<OrderEntity> loginEntityCall = apiInterface.saveOrder(orderEntity);
        loginEntityCall.enqueue(new Callback<OrderEntity>() {
            @Override
            public void onResponse(Call<OrderEntity> call, Response<OrderEntity> response) {

                if (response.body() != null) {

                    OrderEntity ordEntity = response.body();

                    if (ordEntity.getStatus()) {
                        orderPresenter.onOrderSuccess(ordEntity);
                    } else {
                        orderPresenter.onOrderFailure(ordEntity.getMessage());
                    }

                } else {
                    orderPresenter.onOrderFailure(response.message());
                }

            }

            @Override
            public void onFailure(Call<OrderEntity> call, Throwable t) {
                orderPresenter.onOrderError(HttpURLConnection.HTTP_INTERNAL_ERROR, "Oops! Some problem seems to have happened. Please try again");
            }
        });

    }
}
