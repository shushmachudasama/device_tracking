package com.eshiksa.devicetracking.presenter;

import com.eshiksa.devicetracking.pojo.plan.PlanEntity;

/**
 * @author by SHUSHMA on 24/4/18.
 */

public interface PlanPresenter {


    void onPrepareCall();

    void onPlanCall();

    void onPlanSuccess(PlanEntity planEntity);

    void onPlanFailure(String message);

    void onPlanError(int requestCode, String message);
}
