package com.eshiksa.devicetracking.presenterimpl;

import com.eshiksa.devicetracking.pojo.safezone.SafeZoneEntity;
import com.eshiksa.devicetracking.presenter.ModifyZonePresenter;
import com.eshiksa.devicetracking.service.ModifyZoneService;
import com.eshiksa.devicetracking.serviceimpl.activity.ModifyZoneServiceImpl;
import com.eshiksa.devicetracking.view.ModifyZoneView;

/**
 * @author by SHUSHMA on 16/5/18.
 */

public class ModifyZonePresenterImpl implements ModifyZonePresenter {

    private ModifyZoneView modifyZoneView;

    public ModifyZonePresenterImpl(ModifyZoneView modifyZoneView) {
        this.modifyZoneView = modifyZoneView;
    }

    @Override
    public void onPrepareCall() {
        if (modifyZoneView != null)
            modifyZoneView.showProgress();
    }

    @Override
    public void onModifyZoneCall(SafeZoneEntity safeZoneEntity) {
        ModifyZoneService modifyZoneService = new ModifyZoneServiceImpl(this);
        modifyZoneService.modifyZoneCall(safeZoneEntity);
    }

    @Override
    public void onModifyZoneSuccess(SafeZoneEntity safeZoneEntity) {
        if (modifyZoneView != null) {
            modifyZoneView.hideProgress();
            modifyZoneView.onModifyZoneSuccess(safeZoneEntity);
        }
    }

    @Override
    public void onDeleteZoneCall(String zoneId) {
        ModifyZoneService modifyZoneService = new ModifyZoneServiceImpl(this);
        modifyZoneService.deleteZoneCall(zoneId);
    }

    @Override
    public void onDeleteZoneSuccess(SafeZoneEntity safeZoneEntity) {
        if (modifyZoneView != null) {
            modifyZoneView.hideProgress();
            modifyZoneView.onDeleteZoneSuccess(safeZoneEntity);
        }
    }

    @Override
    public void onModifyZoneFailure(String message) {
        modifyZoneView.onFailedMessage(message);
    }

    @Override
    public void onModifyZoneError(int requestCode, String message) {
        if (modifyZoneView != null) {
            modifyZoneView.hideProgress();
            modifyZoneView.onServiceError(message);
        }
    }
}
