package com.eshiksa.devicetracking.service;

import java.util.HashMap;

/**
 * @author by SHUSHMA on 16/5/18.
 */

public interface HistoryService {

    void historyCall(HashMap<String, Object> map);

}
