package com.eshiksa.devicetracking.pojo.devicelocation;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author by AKHILESH on 9/5/18.
 */
public class DeviceCurrentLocationEntity implements Parcelable
{

    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    public final static Parcelable.Creator<DeviceCurrentLocationEntity> CREATOR = new Creator<DeviceCurrentLocationEntity>() {


        @SuppressWarnings({
                "unchecked"
        })
        public DeviceCurrentLocationEntity createFromParcel(Parcel in) {
            return new DeviceCurrentLocationEntity(in);
        }

        public DeviceCurrentLocationEntity[] newArray(int size) {
            return (new DeviceCurrentLocationEntity[size]);
        }

    }
            ;

    protected DeviceCurrentLocationEntity(Parcel in) {
        this.address = ((String) in.readValue((String.class.getClassLoader())));
        this.longitude = ((String) in.readValue((String.class.getClassLoader())));
        this.latitude = ((String) in.readValue((String.class.getClassLoader())));
    }

    public DeviceCurrentLocationEntity() {
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(address);
        dest.writeValue(longitude);
        dest.writeValue(latitude);
    }

    public int describeContents() {
        return 0;
    }

}


