package com.eshiksa.devicetracking.pojo;

import java.io.Serializable;

/**
 * @author by AKHILESH on 5/5/18.
 */
public class ScanData implements Serializable {
    private String deviceId;

    public ScanData(String deviceId) {
        this.deviceId = deviceId;
    }

    public ScanData() {

    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }
}
