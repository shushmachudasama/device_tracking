package com.eshiksa.devicetracking.presenterimpl;

import com.eshiksa.devicetracking.pojo.device.device_list.DeviceListMainEntity;
import com.eshiksa.devicetracking.presenter.DevicePresenter;
import com.eshiksa.devicetracking.service.DeviceService;
import com.eshiksa.devicetracking.serviceimpl.activity.DeviceServiceImpl;
import com.eshiksa.devicetracking.view.DeviceView;

/**
 * @author by SHUSHMA on 11/5/18.
 */

public class DevicePresenterImpl implements DevicePresenter {

    private DeviceView deviceView;

    public DevicePresenterImpl(DeviceView deviceView) {
        this.deviceView = deviceView;
    }

    @Override
    public void onPrepareCall() {
        if (deviceView != null)
            deviceView.showProgress();
    }

    @Override
    public void onDeviceListCall(String username) {
        DeviceService deviceService = new DeviceServiceImpl(this);
        deviceService.onDeviceListServiceCall(username);

    }

    @Override
    public void onDeviceListSuccess(DeviceListMainEntity deviceListMainEntity) {
        if (deviceView != null) {
            deviceView.hideProgress();
            deviceView.onDeviceListSuccess(deviceListMainEntity);
        }
    }

    @Override
    public void onDeviceListFailure(String message) {
        deviceView.onFailedMessage(message);
    }

    @Override
    public void onDeviceListError(int requestCode, String message) {
        if (deviceView != null) {
            deviceView.hideProgress();
            deviceView.onServiceError(message);
        }
    }
}
