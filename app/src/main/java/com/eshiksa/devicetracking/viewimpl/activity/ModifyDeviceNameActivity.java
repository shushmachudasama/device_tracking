package com.eshiksa.devicetracking.viewimpl.activity;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.eshiksa.devicetracking.R;
import com.eshiksa.devicetracking.pojo.device.device_list.DeviceList;
import com.eshiksa.devicetracking.pojo.device.updatenickname.UpdateNickNameEntity;
import com.eshiksa.devicetracking.serviceimpl.ApiClient;
import com.eshiksa.devicetracking.serviceimpl.ApiInterface;
import com.eshiksa.devicetracking.util.ShowAlertDialog;
import com.eshiksa.devicetracking.viewimpl.adapter.ModifyAdapter;
import com.eshiksa.devicetracking.viewimpl.fragment.ProgressDialogFragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ModifyDeviceNameActivity extends AppCompatActivity implements ModifyAdapter.OnImgEditClickListener {

    @BindView(R.id.recycler_modify)
    RecyclerView recyclerModify;

    private List<DeviceList> mList;
    Dialog deviceDialog;
    EditText edNickName;
    private ProgressDialogFragment progressDialogFragment;
    ModifyAdapter modifyAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modify_device_name);
        ButterKnife.bind(ModifyDeviceNameActivity.this);
        progressDialogFragment = new ProgressDialogFragment();

        deviceDialog = new Dialog(ModifyDeviceNameActivity.this);


        recyclerModify.setLayoutManager(new LinearLayoutManager(this));
        mList = new ArrayList<>();

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            mList = bundle.getParcelableArrayList("mList");
        }

        if (mList.size() > 0) {
            modifyAdapter = new ModifyAdapter(mList, this,this);
//            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(ModifyDeviceNameActivity.this);
//            recyclerModify.setLayoutManager(layoutManager);
            recyclerModify.setAdapter(modifyAdapter);
        }

    }

    @OnClick({R.id.txtCancel})
    public void cancelClick(View view) {

        switch (view.getId()) {

            case R.id.txtCancel:
                finish();
                break;
        }
    }


    @Override
    public void onImgEditClick(final DeviceList deviceListEntity) {
        deviceDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        deviceDialog.setContentView(R.layout.dialog_modify_nick_name);
        deviceDialog.setCancelable(false);
        deviceDialog.show();

        ImageView cancel = deviceDialog.findViewById(R.id.cancel);
        Button mBtnSave= deviceDialog.findViewById(R.id.btn_save);
        edNickName = deviceDialog.findViewById(R.id.edNickName);

        edNickName.setText(deviceListEntity.getNickName());

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deviceDialog.dismiss();
            }
        });

        mBtnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edNickName.getText().toString().isEmpty()){
                    Toast.makeText(ModifyDeviceNameActivity.this, "Nickname Can't be Empty !!!", Toast.LENGTH_SHORT).show();
                } else {
                    deviceDialog.dismiss();
                    nickNameUpdate(edNickName.getText().toString(),deviceListEntity.getDeviceNumber());
                }
            }
        });


    }

    private void nickNameUpdate(String nickName, String deviceNumber) {
        progressDialogFragment.show(getFragmentManager(), "Loading.....");
        HashMap<String, Object> map = new HashMap<>();
        map.put("deviceNumber", deviceNumber);
        map.put("nickName", nickName);

        ApiInterface apiInterface = ApiClient.getClient(null).create(ApiInterface.class);
        Call<UpdateNickNameEntity> call = apiInterface.getNicknameUpdate(map);
        call.enqueue(new Callback<UpdateNickNameEntity>() {
            @Override
            public void onResponse(Call<UpdateNickNameEntity> call, Response<UpdateNickNameEntity> response) {
                if (response.body()!=null){
                    progressDialogFragment.dismiss();
                    if (response.body().getStatus() == true){
                        modifyAdapter.notifyDataSetChanged();
                        ShowAlertDialog.showAlert(ModifyDeviceNameActivity.this,"Congrats  !!!  Your Nick Name Updated...","OK");
                    }
                }
            }

            @Override
            public void onFailure(Call<UpdateNickNameEntity> call, Throwable t) {

            }
        });
    }

}
