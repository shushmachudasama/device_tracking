package com.eshiksa.devicetracking.viewimpl.fragment;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.eshiksa.devicetracking.R;
import com.eshiksa.devicetracking.pojo.LoginEntity;
import com.eshiksa.devicetracking.presenter.LoginPresenter;
import com.eshiksa.devicetracking.presenterimpl.LoginPresenterImpl;
import com.eshiksa.devicetracking.util.ShowAlertDialog;
import com.eshiksa.devicetracking.view.LoginView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ForgotPasswordFragment extends Fragment implements LoginView {

    private ProgressDialogFragment progressDialogFragment;

    @BindView(R.id.btnBack)
    ImageView btnBack;
    @BindView(R.id.btn_submit)
    Button btnSubmit;
    @BindView(R.id.forgotEmail)
    EditText forgotEmail;
    @BindView(R.id.constraintLogin)
    LinearLayout layout;
    @BindView(R.id.relEroorMsg)
    RelativeLayout relErrorMsg;
    @BindView(R.id.txtErrorMsg)
    TextView txtErrorMsg;

    public ForgotPasswordFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_forgot_password, container, false);
        ButterKnife.bind(this, view);
        progressDialogFragment = new ProgressDialogFragment();
        setTypeFaceLayout();
        return view;
    }

    @OnClick({R.id.btnBack, R.id.btn_submit})
    public void onBtnClick(View view) {

        switch (view.getId()) {

            case R.id.btnBack:
                openLoginFragment();
                break;

            case R.id.btn_submit:
                submitTask();
                break;
        }
    }

    private void openLoginFragment() {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, new LoginFragment()).commit();
    }

    /**
     * Method to perform submit task to get new password
     */
    private void submitTask() {

        if (!(forgotEmail.getText().toString().isEmpty())) {
            LoginPresenter loginPresenter = new LoginPresenterImpl(this);
            loginPresenter.onEmailCall(forgotEmail.getText().toString());
        } else {
            relErrorMsg.setVisibility(View.VISIBLE);
            txtErrorMsg.setText("Please enter valid email address");
        }

    }

    /*
     Adding Type face to @Field
    */
    private void setTypeFaceLayout() {

        Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/JosefinSans-Regular.ttf");
        btnSubmit.setTypeface(typeface);
        forgotEmail.setTypeface(typeface);
        txtErrorMsg.setTypeface(typeface);
    }

    @Override
    public void showProgress() {
        if (progressDialogFragment != null) {
            progressDialogFragment.show(getFragmentManager(), getResources().getString(R.string.msg_progress_dialog));
        }
    }

    @Override
    public void hideProgress() {
        if (progressDialogFragment != null)
            progressDialogFragment.dismiss();
    }

    @Override
    public void onLoginSuccess(LoginEntity loginResponse) {
        if (loginResponse.getStatus()) {
            Toast.makeText(getActivity(), loginResponse.getMessage(), Toast.LENGTH_SHORT).show();
            openLoginFragment();

        } else {
            Toast.makeText(getActivity(), loginResponse.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onFailedMessage(String message) {
        relErrorMsg.setVisibility(View.VISIBLE);
        txtErrorMsg.setText(message);
        hideProgress();
    }

    @Override
    public void onServiceError(String status) {
        ShowAlertDialog.showAlert(getActivity(), status, "OK");
        hideProgress();
    }
}
