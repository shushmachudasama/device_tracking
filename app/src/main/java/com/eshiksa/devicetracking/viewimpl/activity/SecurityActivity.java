package com.eshiksa.devicetracking.viewimpl.activity;

import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.eshiksa.devicetracking.R;
import com.eshiksa.devicetracking.pojo.safezone.Devicezone;
import com.eshiksa.devicetracking.pojo.safezone.SafeZoneEntity;
import com.eshiksa.devicetracking.presenter.SafeZonePresenter;
import com.eshiksa.devicetracking.presenterimpl.SafeZonePresenterImpl;
import com.eshiksa.devicetracking.util.Constant;
import com.eshiksa.devicetracking.util.SharePrefrancClass;
import com.eshiksa.devicetracking.util.ShowAlertDialog;
import com.eshiksa.devicetracking.view.SafezoneView;
import com.eshiksa.devicetracking.viewimpl.fragment.ProgressDialogFragment;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SecurityActivity extends AppCompatActivity implements
        OnMapReadyCallback, GoogleMap.OnMapClickListener,
        GoogleMap.OnMarkerClickListener, SafezoneView,
        GoogleMap.OnInfoWindowClickListener,
        GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks, LocationListener {

    private static final long INTERVAL = 1000 * 10; //10 sec
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final int REQUEST_CHECK_SETTINGS = 57;

    private GoogleMap mGoogleMap;
    private ProgressDialogFragment progressDialogFragment;
    private Double latitude;
    private Double longitude;
    private int distance;
    private Marker mapMarker;
    private boolean isFirst = true;
    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    Location mCurrentLocation;

    @BindView(R.id.addSafeZone)
    LinearLayout addSafeZone;
    @BindView(R.id.viewSafeZone)
    LinearLayout viewSafeZone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_security);

        ButterKnife.bind(SecurityActivity.this);
        progressDialogFragment = new ProgressDialogFragment();
        if (!isGooglePlayServicesAvailable()) {
            finish();
        }
        createLocationRequest();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        SupportMapFragment fm = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.security_map);
        fm.getMapAsync(this);

//        viewZone();

    }

    // maps integration method
    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(INTERVAL);
        mLocationRequest.setFastestInterval(0);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    @OnClick({R.id.addSafeZone, R.id.imageBack})
    public void addViewSafeZone(View view) {

        switch (view.getId()) {

            case R.id.addSafeZone:
                String title = null;
                addZone(title);
                break;

            case R.id.viewSafeZone:
                if (Constant.DEVICE_NUMBER != "") {
                    viewZone();
                } else {
                    Toast.makeText(this, "No safe zone available.", Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.imageBack:
                onBackPressed();
                break;
        }

    }

    /**
     * Method to view list of safe zones
     */
    private void viewZone() {
        SafeZonePresenter safeZonePresenter = new SafeZonePresenterImpl(this);
        safeZonePresenter.onSafeZoneListCall(SharePrefrancClass.getInstance(SecurityActivity.this).getPref(Constant.USERNAME), Constant.DEVICE_NUMBER);
    }

    private boolean isGooglePlayServicesAvailable() {
        GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();
        int status1 = googleAPI.isGooglePlayServicesAvailable(this);
        if (ConnectionResult.SUCCESS == status1) {
            return true;
        } else {
            googleAPI.getErrorDialog(this, status1,
                    PLAY_SERVICES_RESOLUTION_REQUEST).show();
            return false;
        }
    }


    /**
     * Method to add safe zone in app
     *
     * @param title
     */
    private void addZone(String title) {

        final Dialog dialog = new Dialog(SecurityActivity.this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.add_zone);
        dialog.setCancelable(false);
        dialog.show();

        ImageView cancel = dialog.findViewById(R.id.cancel);
        final EditText altPlaceName = dialog.findViewById(R.id.ed_alt_place);
        Button btn_save = dialog.findViewById(R.id.btn_save);
        SeekBar seekBar = dialog.findViewById(R.id.seekBar);
        final TextView tvSeekDist = dialog.findViewById(R.id.seekDistance);

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                int incrementBy = 20;
                progress = ((int) Math.round(progress / incrementBy)) * incrementBy;
                seekBar.setProgress(progress);
                tvSeekDist.setText(progress + "m");
                distance = progress;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        final PlaceAutocompleteFragment autocompleteFragment = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);

        final String[] placeName = new String[1];

        AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
                .setTypeFilter(AutocompleteFilter.TYPE_FILTER_NONE)
                .build();
        autocompleteFragment.setFilter(typeFilter);
        if (title != null) {
            autocompleteFragment.setText(title);
        }
        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {

                placeName[0] = String.valueOf(place.getName());
                latitude = place.getLatLng().latitude;
                longitude = place.getLatLng().longitude;
                // TODO: Get info about the selected place.
                Log.i("Place", "Place: " + place.getName());//get place details here
            }

            @Override
            public void onError(Status status) {
                // TODO: Handle the error.
                Log.i("Error", "An error occurred: " + status);
            }
        });

        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String place = placeName[0];
                String altPlace = altPlaceName.getText().toString();

                if (!(altPlace.isEmpty()) && (place != null)) {
                    onDestroyView();
                    if (distance > 20) {
                        addSafeZoneTask(place, altPlace);
                        dialog.dismiss();
                    } else {
                        Toast.makeText(SecurityActivity.this, "Distance should be greater than 20m", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(SecurityActivity.this, "Please enter alternate place name.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onDestroyView();
                dialog.dismiss();
            }
        });
    }

    /**
     * Method to add safe zone
     * @param place
     * @param altPlace
     */
    private void addSafeZoneTask(String place, String altPlace) {

        SafeZoneEntity safeZoneEntity = new SafeZoneEntity();
        safeZoneEntity.setUsername(SharePrefrancClass.getInstance(this).getPref(Constant.USERNAME));
        safeZoneEntity.setLatitude(String.valueOf(latitude));
        safeZoneEntity.setLongitude(String.valueOf(longitude));
        safeZoneEntity.setDeviceNumber(Constant.DEVICE_NUMBER);
        safeZoneEntity.setDistance(String.valueOf(distance));
        safeZoneEntity.setPlaceName(place);
        safeZoneEntity.setAlternatePlaceName(altPlace);

        SafeZonePresenter safeZonePresenter = new SafeZonePresenterImpl(this);
        safeZonePresenter.onAddSafeZoneCall(safeZoneEntity);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mGoogleMap = googleMap;
        mGoogleMap.setOnMapClickListener(this);
        mGoogleMap.setOnMarkerClickListener(this);

        if (mGoogleMap != null) {
            mGoogleMap.getUiSettings().setZoomControlsEnabled(false);
            mGoogleMap.setOnInfoWindowClickListener(this);

        }

    }

    @Override
    public void onMapClick(LatLng latLng) {

        mGoogleMap.clear();
        addMarker(latLng);
//        new ReverseGeocodingTask(getBaseContext()).execute(latLng);

    }

    private void addMarker(LatLng latLng) {

        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        List<Address> addresses = new ArrayList<>();
        try {
            addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }

        android.location.Address address = addresses.get(0);

        MarkerOptions options = new MarkerOptions();
        options.icon(Constant.bitmapDescriptorFromVector(this, R.drawable.ic_maps));
        LatLng currentLatLng = new LatLng(latLng.latitude, latLng.longitude);
        options.position(currentLatLng)
                .title(address.getAddressLine(0));
        mGoogleMap.addMarker(options);
        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 13));

    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        addZone(marker.getTitle());
        return false;
    }

    public void onDestroyView() {
        FragmentManager fm = getFragmentManager();
        Fragment fragment = (fm.findFragmentById(R.id.place_autocomplete_fragment));
        FragmentTransaction ft = fm.beginTransaction();
        ft.remove(fragment);
        ft.commit();
    }

    @Override
    public void showProgress() {
        if (progressDialogFragment != null) {
            progressDialogFragment.show(this.getFragmentManager(), getResources().getString(R.string.msg_progress_dialog));
        }
    }

    @Override
    public void hideProgress() {
        if (progressDialogFragment != null) {
            progressDialogFragment.dismiss();
        }
    }

    @Override
    public void onSafeZoneSuccess(SafeZoneEntity safeZoneEntity) {
        if (safeZoneEntity.getStatus()) {
            Toast.makeText(this, safeZoneEntity.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onSafeZoneListSuccess(SafeZoneEntity safeZoneEntity) {

        if (safeZoneEntity.getStatus()) {

            if (safeZoneEntity.getDevicezones() != null) {

//                drawCircleOnMap(safeZoneEntity.getDevicezones());
                Intent intent = new Intent(SecurityActivity.this, SafeZoneActivity.class);
                intent.putParcelableArrayListExtra("zones", (ArrayList<? extends Parcelable>) safeZoneEntity.getDevicezones());
                startActivity(intent);
            } else {
                Toast.makeText(this, "No safe zone added.", Toast.LENGTH_SHORT).show();
            }

        } else {
            Toast.makeText(this, safeZoneEntity.getMessage(), Toast.LENGTH_SHORT).show();
        }

    }

    private void drawCircleOnMap(List<Devicezone> devicezones) {

        if (devicezones.size() > 0) {

            for (int i = 0; i < devicezones.size(); i++) {
                Double lat = devicezones.get(i).getLatitude();
                Double lon = devicezones.get(i).getLongitude();
                addMarker(new LatLng(lat, lon));
                Circle circle = mGoogleMap.addCircle(new CircleOptions()
                        .center(new LatLng(lat,lon))
                        .radius(2000)
                        .strokeColor(0x50c4ecff)
                        .fillColor(0x50c4ecff));
            }
        }

    }

    @Override
    public void onFailedMessage(String message) {
        ShowAlertDialog.showAlert(this, message, "OK");
        hideProgress();
    }

    @Override
    public void onServiceError(String message) {
        ShowAlertDialog.showAlert(this, message, "OK");
        hideProgress();
    }

    protected void startLocationUpdates() {
        try {
            PendingResult<Status> pendingResult =
                    LocationServices.FusedLocationApi.requestLocationUpdates(
                            mGoogleApiClient, mLocationRequest, this);
        } catch (SecurityException se) {
            se.printStackTrace();
        }

//        Log.d(TAG, "Location update started ..............: ");
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        startLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        mCurrentLocation = location;
        if (mapMarker != null) {
            if (location.getAccuracy() < 50f) {
                mapMarker.remove();
                addMarker(new LatLng(location.getLatitude(), location.getLongitude()));
            }
        } else {
            addMarker(new LatLng(location.getLatitude(), location.getLongitude()));
        }
    }

    @Override
    public void onInfoWindowClick(Marker marker) {

    }

    // maps integration
    @Override
    public void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopLocationUpdates();
    }

    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
    }

    @Override
    public void onResume() {
        super.onResume();
        displayLocationSettingsRequest(this);
    }

    private void displayLocationSettingsRequest(Context context) {
        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API).build();
        googleApiClient.connect();

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(10000 / 2);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final com.google.android.gms.common.api.Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        if (mGoogleApiClient.isConnected()) {
                            startLocationUpdates();
//            Log.d(TAG, "Location update resumed .....................");
                        }
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
//                        Log.i(TAG, "Location settings are not satisfied. Show the user a dialog to upgrade location settings ");

                        try {
                            // Show the dialog by calling startResolutionForResult(), and check the result
                            // in onActivityResult().
                            status.startResolutionForResult(SecurityActivity.this, REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
//                            Log.i(TAG, "PendingIntent unable to execute request.");
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
//                        Log.i(TAG, "Location settings are inadequate, and cannot be fixed here. Dialog not created.");
                        break;
                }
            }
        });
    }

}
