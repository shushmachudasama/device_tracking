package com.eshiksa.devicetracking.service;

import com.eshiksa.devicetracking.pojo.order.OrderEntity;

/**
 * @author by SHUSHMA on 24/4/18.
 */

public interface OrderService {

    void orderCall(OrderEntity orderEntity);

}
