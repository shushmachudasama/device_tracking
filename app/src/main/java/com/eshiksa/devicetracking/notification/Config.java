package com.eshiksa.devicetracking.notification;

/**
 * @author by AKHILESH on 08/02/18.
 */

public class Config {
//    SHA FINGERPRINT
//    6adbf1c471b2b79e83d7018d61bedc597d573fd4
    // global topic to receive app wide push notifications
    public static final String TOPIC_GLOBAL = "global";

    // broadcast receiver intent filters
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String PUSH_NOTIFICATION = "pushNotification";

    // id to handle the notification in the notification tray
    public static final int NOTIFICATION_ID = 100;
    public static final int NOTIFICATION_ID_BIG_IMAGE = 101;

    public static final String SHARED_PREF = "ah_firebase";
}
