
package com.eshiksa.devicetracking.pojo.plan;

import java.util.ArrayList;
import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PlanEntity implements Parcelable {

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("devicePlansVOs")
    @Expose
    private List<DevicePlansVO> devicePlansVOs = new ArrayList<>();
    @SerializedName("status")
    @Expose
    private Boolean status;
    public final static Creator<PlanEntity> CREATOR = new Creator<PlanEntity>() {


        @SuppressWarnings({
                "unchecked"
        })
        public PlanEntity createFromParcel(Parcel in) {
            return new PlanEntity(in);
        }

        public PlanEntity[] newArray(int size) {
            return (new PlanEntity[size]);
        }

    };

    protected PlanEntity(Parcel in) {
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.devicePlansVOs, (DevicePlansVO.class.getClassLoader()));
        this.status = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
    }

    public PlanEntity() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DevicePlansVO> getDevicePlansVOs() {
        return devicePlansVOs;
    }

    public void setDevicePlansVOs(List<DevicePlansVO> devicePlansVOs) {
        this.devicePlansVOs = devicePlansVOs;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(message);
        dest.writeList(devicePlansVOs);
        dest.writeValue(status);
    }

    public int describeContents() {
        return 0;
    }

}
