package com.eshiksa.devicetracking.serviceimpl;

import com.eshiksa.devicetracking.pojo.CurrentLocationEntity;
import com.eshiksa.devicetracking.pojo.LoginEntity;
import com.eshiksa.devicetracking.pojo.device.AddDeviceEntity;
import com.eshiksa.devicetracking.pojo.device.device_list.DeviceListMainEntity;
import com.eshiksa.devicetracking.pojo.device.updatenickname.UpdateNickNameEntity;
import com.eshiksa.devicetracking.pojo.devicelocation.DeviceCurrentLocationEntity;
import com.eshiksa.devicetracking.pojo.history.HistoryEntity;
import com.eshiksa.devicetracking.pojo.order.OrderEntity;
import com.eshiksa.devicetracking.pojo.order.OrderStatusEntity;
import com.eshiksa.devicetracking.pojo.paymentpage.PaymentResponseEntity;
import com.eshiksa.devicetracking.pojo.plan.PlanEntity;
import com.eshiksa.devicetracking.pojo.safezone.SafeZoneEntity;
import com.eshiksa.devicetracking.pojo.setting.CheckSwitchEntity;
import com.eshiksa.devicetracking.pojo.setting.UpdateNotificationEntity;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * @author by SHUSHMA on 23/4/18.
 */

public interface ApiInterface {

    @Headers("Content-Type: application/json")
    @POST("tracking/Login")
    Call<LoginEntity> loginUser(@Body HashMap<String, Object> body);

    @Headers("Content-Type: application/json")
    @POST("tracking/signUp")
    Call<LoginEntity> signUpUser(@Body HashMap<String, Object> signUpMap);

    @GET("tracking/getDevicePlans")
    Call<PlanEntity> getPlans();

    @Headers("Content-Type: application/json")
    @POST("tracking/saveOrder")
    Call<OrderEntity> saveOrder(@Body OrderEntity orderEntity);

    @Headers("Content-Type: application/json")
    @POST("tracking/purchaseDevicePlans/")
    Call<PaymentResponseEntity> gotoPaymentPage(@Body HashMap<String, Object> entity);

    @Headers("Content-Type: application/json")
    @POST("tracking/forgotPassword")
    Call<LoginEntity> forgotPassword(@Body HashMap<String, Object> emailMap);

    @Headers("Content-Type: application/json")
    @POST("tracking/AddDevice")
    Call<AddDeviceEntity> addDeviceEntity(@Body HashMap<String,Object> body);

    @Headers("Content-Type: application/json")
    @POST("tracking/getDeviceListByuserName")
    Call<DeviceListMainEntity> getDeviceList(@Body HashMap<String,Object> username);

    @Headers("Content-Type: application/json")
    @POST("safeZone/addSafeZone")
    Call<SafeZoneEntity> saveSafeZone(@Body SafeZoneEntity safeZoneEntity);

    @Headers("Content-Type: application/json")
    @POST("location/currentLocation")
    Call<CurrentLocationEntity> getCurrentLocation(@Body HashMap<String, Object> deviceId);

    @Headers("Content-Type: application/json")
    @POST("safeZone/SafeZoneBy")
    Call<SafeZoneEntity> getSafeZoneList(@Body HashMap<String, Object> map);

    @Headers("Content-Type: application/json")
    @POST("tracking/updateNickName")
    Call<UpdateNickNameEntity> getNicknameUpdate(@Body HashMap<String,Object> body);

    @Headers("Content-Type: application/json")
    @POST("location/locationHistory")
    Call<HistoryEntity> getHistory(@Body HashMap<String, Object> map);

    @Headers("Content-Type: application/json")
    @POST("safeZone/updateSafeZone")
    Call<SafeZoneEntity> updateSafeZone(@Body SafeZoneEntity safeZoneEntity);

    @Headers("Content-Type: application/json")
    @POST("safeZone/DeleteSafeZone")
    Call<SafeZoneEntity> deleteSafeZone(@Body HashMap<String, Object> map);

    @Headers("Content-Type: application/json")
    @POST("tracking/processingPayment")
    Call<OrderEntity> gotopg(@Body HashMap<String, Object> hashMap);

    @Headers("Content-Type: application/json")
    @POST("tracking/checkOrderStatus")
    Call<OrderStatusEntity> checkOrderStatus(@Body HashMap<String, Object> statusMap);

    @Headers("Content-Type: application/json")
    @POST("fireBase/checkNotification")
    Call<CheckSwitchEntity> checkswitchEnabled(@Body HashMap<String, Object> hashMap);

    @Headers("Content-Type: application/json")
    @POST("fireBase/updateNotification")
    Call<UpdateNotificationEntity> updateNotification(@Body HashMap<String, Object> hashMap);

    @Headers("Content-Type: application/json")
    @POST("fireBase/fireBaseRegistration")
    Call<Object> registerFirebaseNotification(@Body HashMap<String, Object> map);
}
