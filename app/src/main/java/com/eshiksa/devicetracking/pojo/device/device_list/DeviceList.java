package com.eshiksa.devicetracking.pojo.device.device_list;

/**
 * @author by AKHILESH on 9/5/18.
 */

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DeviceList implements Parcelable {

    @SerializedName("id")
    @Expose
    private Long id;
    @SerializedName("deviceNumber")
    @Expose
    private String deviceNumber;
//    @SerializedName("deviceId")
//    @Expose
//    private Long deviceId;
    @SerializedName("nickName")
    @Expose
    private String nickName;
    @SerializedName("mobileNumber")
    @Expose
    private String mobileNumber;
    @SerializedName("username")
    @Expose
    private String username;
    public final static Parcelable.Creator<DeviceList> CREATOR = new Creator<DeviceList>() {

        @SuppressWarnings({
                "unchecked"
        })
        public DeviceList createFromParcel(Parcel in) {
            return new DeviceList(in);
        }

        public DeviceList[] newArray(int size) {
            return (new DeviceList[size]);
        }

    };

    protected DeviceList(Parcel in) {
        this.id = ((Long) in.readValue((Long.class.getClassLoader())));
//        this.deviceId = ((Long) in.readValue((Long.class.getClassLoader())));
        this.deviceNumber = ((String) in.readValue((String.class.getClassLoader())));
        this.nickName = ((String) in.readValue((String.class.getClassLoader())));
        this.mobileNumber = ((String) in.readValue((String.class.getClassLoader())));
        this.username = ((String) in.readValue((String.class.getClassLoader())));
    }

    public DeviceList() {
    }

//    public Long getDeviceId() {
//        return deviceId;
//    }
//
//    public void setDeviceId(Long deviceId) {
//        this.deviceId = deviceId;
//    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDeviceNumber() {
        return deviceNumber;
    }

    public void setDeviceNumber(String deviceNumber) {
        this.deviceNumber = deviceNumber;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
//        dest.writeValue(deviceId);
        dest.writeValue(deviceNumber);
        dest.writeValue(nickName);
        dest.writeValue(mobileNumber);
        dest.writeValue(username);
    }

    public int describeContents() {
        return 0;
    }

}
