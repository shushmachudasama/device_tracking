package com.eshiksa.devicetracking.viewimpl.activity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.IntentSender;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.eshiksa.devicetracking.R;
import com.eshiksa.devicetracking.pojo.history.HistoryEntity;
import com.eshiksa.devicetracking.pojo.history.ListOfLocation;
import com.eshiksa.devicetracking.presenter.HistoryPresenter;
import com.eshiksa.devicetracking.presenterimpl.HistoryPresenterImpl;
import com.eshiksa.devicetracking.util.Constant;
import com.eshiksa.devicetracking.util.SharePrefrancClass;
import com.eshiksa.devicetracking.util.ShowAlertDialog;
import com.eshiksa.devicetracking.view.HistoryView;
import com.eshiksa.devicetracking.viewimpl.fragment.ProgressDialogFragment;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HistoryActivity extends AppCompatActivity implements
        DatePickerDialog.OnDateSetListener, HistoryView, OnMapReadyCallback,
        GoogleMap.OnInfoWindowClickListener, GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks, LocationListener {

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final long INTERVAL = 1000 * 10; //10 sec
    private static final int REQUEST_CHECK_SETTINGS = 57;

    private Calendar currentCalender;
    private ProgressDialogFragment progressDialogFragment;
    private GoogleMap googleMap;
    private Marker mapMarker;
    private boolean isFirst = true;
    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    Location mCurrentLocation;

    @BindView(R.id.imageBack)
    ImageView imageBack;
    @BindView(R.id.btnFrom)
    Button btnFrom;
    @BindView(R.id.btnTo)
    Button btnTo;
    @BindView(R.id.btnDate)
    Button btnDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        ButterKnife.bind(HistoryActivity.this);
        currentCalender = Calendar.getInstance();
        progressDialogFragment = new ProgressDialogFragment();

        if (!isGooglePlayServicesAvailable()) {
            finish();
        }
        createLocationRequest();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        SupportMapFragment fm = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.history_map);
        fm.getMapAsync(this);

    }

    @OnClick({R.id.imageBack, R.id.btnHistRoute, R.id.btnFrom, R.id.btnTo, R.id.btnDate})
    public void historyClick(View view) {

        switch (view.getId()) {
            case R.id.imageBack:
                onBackPressed();
                break;

            case R.id.btnHistRoute:
                getHistoricalRoute();
                break;

            case R.id.btnFrom:
                selectTime("from");
                break;

            case R.id.btnTo:
                selectTime("to");
                break;

            case R.id.btnDate:
                selectDate();
                break;
        }
    }

    /**
     * Method to get historical route
     */
    private void getHistoricalRoute() {

        if (isValid()) {

            String username = SharePrefrancClass.getInstance(this).getPref(Constant.USERNAME);
            String fromDate = btnDate.getText().toString() + " " + btnFrom.getText().toString() + ":00";
            String toDate = btnDate.getText().toString() + " " + btnTo.getText().toString() + ":00";

            HashMap<String, Object> map = new HashMap<>();
            map.put("userName", username);
            map.put("fromDate", fromDate);
            map.put("toDate", toDate);
            map.put("deviceNumber", Constant.DEVICE_NUMBER);

            HistoryPresenter historyPresenter = new HistoryPresenterImpl(this);
            historyPresenter.onHistoryCall(map);

        }
    }

    /**
     * Method is used for validation
     *
     * @return
     */
    private boolean isValid() {
        boolean isValid = true;
        String msg = null;
        if (btnFrom.getText().toString().equals("HH:mm:ss")) {
            isValid = false;
            msg = "Please select time (from).";
        } else if (btnTo.getText().toString().equals("HH:mm:ss")) {
            isValid = false;
            msg = "Please select time (to).";
        } else if (btnDate.getText().toString().equals("yyyy-MM-dd")) {
            isValid = false;
            msg = "Please select date.";
        }

        if (!isValid) {
            Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        }
        return isValid;
    }

    /**
     * Method to select from and to time for history
     *
     * @param id
     */
    private void selectTime(final String id) {
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                String shour = String.valueOf(selectedHour);
                String sMin = String.valueOf(selectedMinute);

                if(shour.length() == 1){
                    shour = "0" + shour;
                }
                if(sMin.length() == 1){
                    sMin = "0" + sMin;
                }
                if (id.equalsIgnoreCase("from")) {
                    btnFrom.setText(shour + ":" + sMin);
                } else if (id.equalsIgnoreCase("to")) {
                    btnTo.setText(shour + ":" + sMin);
                }
            }
        }, hour, minute, true);//Yes 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();
    }

    /**
     * Method to show date picker to select date
     */
    private void selectDate() {

        DatePickerDialog datePickerDialog = new DatePickerDialog(this, this,
                currentCalender.get(Calendar.YEAR), currentCalender.get(Calendar.MONTH),
                currentCalender.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.getDatePicker();
        datePickerDialog.show();
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        currentCalender.set(Calendar.YEAR, year);
        currentCalender.set(Calendar.MONTH, month);
        currentCalender.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        updateLabel();
    }

    /**
     * Method to update date button value
     */
    private void updateLabel() {

        String myFormat = "yyyy-MM-dd";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        btnDate.setText(sdf.format(currentCalender.getTime()));
    }

    @Override
    public void showProgress() {
        if (progressDialogFragment != null) {
            progressDialogFragment.show(this.getFragmentManager(), getResources().getString(R.string.msg_progress_dialog));
        }
    }

    @Override
    public void hideProgress() {
        if (progressDialogFragment != null) {
            progressDialogFragment.dismiss();
        }
    }

    @Override
    public void onHistorySuccess(HistoryEntity historyEntity) {

        List<ListOfLocation> listOfLocations = historyEntity.getListOfLocation();
        if (listOfLocations != null) {

            if (listOfLocations.size() > 0) {

                for (int i = 0; i < listOfLocations.size(); i++) {
                    MarkerOptions options = new MarkerOptions();
                    options.icon(Constant.bitmapDescriptorFromVector(this, R.drawable.ic_pin));

                    LatLng currentLatLng = new LatLng(Double.valueOf(listOfLocations.get(i).getLattitude()),
                            Double.valueOf(listOfLocations.get(i).getLongitude()));
                    options.position(currentLatLng).title("Visited place: " + i);
                    mapMarker = googleMap.addMarker(options);

                }
            }
        }

        Toast.makeText(this, "Location history obtained successfully.", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailedMessage(String message) {
        ShowAlertDialog.showAlert(this, message, "OK");
        hideProgress();
    }

    @Override
    public void onServiceError(String message) {
        ShowAlertDialog.showAlert(this, message, "OK");
        hideProgress();
    }

    @Override
    public void onMapReady(GoogleMap gMap) {
        googleMap = gMap;
        if (googleMap != null) {
            googleMap.getUiSettings().setZoomControlsEnabled(false);
            googleMap.setOnInfoWindowClickListener(this);

        }
    }

    @Override
    public void onInfoWindowClick(Marker marker) {

    }

    // maps integration method
    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(INTERVAL);
        mLocationRequest.setFastestInterval(0);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    private boolean isGooglePlayServicesAvailable() {
        GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();
        int status1 = googleAPI.isGooglePlayServicesAvailable(this);
        if (ConnectionResult.SUCCESS == status1) {
            return true;
        } else {
            googleAPI.getErrorDialog(this, status1,
                    PLAY_SERVICES_RESOLUTION_REQUEST).show();
            return false;
        }
    }

    protected void startLocationUpdates() {
        try {
            PendingResult<Status> pendingResult =
                    LocationServices.FusedLocationApi.requestLocationUpdates(
                            mGoogleApiClient, mLocationRequest, this);
        } catch (SecurityException se) {
            se.printStackTrace();
        }

//        Log.d(TAG, "Location update started ..............: ");
    }

    private void addMarker() {
        MarkerOptions options = new MarkerOptions();
        options.icon(Constant.bitmapDescriptorFromVector(this, R.drawable.ic_maps));

        LatLng currentLatLng = new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
        options.position(currentLatLng)
                .title("My Location")
                .snippet("Welcome Its you");
        mapMarker = googleMap.addMarker(options);
        if (isFirst) {
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 13));
            isFirst = false;
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        startLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    // maps integration
    @Override
    public void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();
    }

    @Override
    public void onLocationChanged(Location location) {
        mCurrentLocation = location;
        if (mapMarker != null) {
            if (location.getAccuracy() < 50f) {
                mapMarker.remove();
                addMarker();
            }
        } else {
            addMarker();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopLocationUpdates();
    }

    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
    }

    @Override
    public void onResume() {
        super.onResume();
        displayLocationSettingsRequest(this);
    }

    private void displayLocationSettingsRequest(Context context) {
        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API).build();
        googleApiClient.connect();

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(10000 / 2);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final com.google.android.gms.common.api.Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        if (mGoogleApiClient.isConnected()) {
                            startLocationUpdates();
//            Log.d(TAG, "Location update resumed .....................");
                        }
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
//                        Log.i(TAG, "Location settings are not satisfied. Show the user a dialog to upgrade location settings ");

                        try {
                            // Show the dialog by calling startResolutionForResult(), and check the result
                            // in onActivityResult().
                            status.startResolutionForResult(HistoryActivity.this, REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
//                            Log.i(TAG, "PendingIntent unable to execute request.");
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
//                        Log.i(TAG, "Location settings are inadequate, and cannot be fixed here. Dialog not created.");
                        break;
                }
            }
        });
    }

}
