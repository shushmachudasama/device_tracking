package com.eshiksa.devicetracking.presenter;

import com.eshiksa.devicetracking.pojo.safezone.SafeZoneEntity;

/**
 * @author by SHUSHMA on 12/5/18.
 */

public interface SafeZonePresenter {

    void onPrepareCall();

    void onAddSafeZoneCall(SafeZoneEntity safeZoneEntity);

    void onAddSafeZoneSuccess(SafeZoneEntity safeZoneEntity);

    void onSafeZoneListCall(String username, String deviceId);

    void onSafeZoneListSuccess(SafeZoneEntity safeZoneEntity);

    void onSafeZoneFailure(String message);

    void onSafeZoneError(int requestCode, String message);
}
