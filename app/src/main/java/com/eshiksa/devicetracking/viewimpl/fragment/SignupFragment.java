package com.eshiksa.devicetracking.viewimpl.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.eshiksa.devicetracking.R;
import com.eshiksa.devicetracking.pojo.LoginEntity;
import com.eshiksa.devicetracking.presenter.LoginPresenter;
import com.eshiksa.devicetracking.presenterimpl.LoginPresenterImpl;
import com.eshiksa.devicetracking.util.ShowAlertDialog;
import com.eshiksa.devicetracking.view.LoginView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignupFragment extends Fragment implements LoginView, View.OnFocusChangeListener {

    @BindView(R.id.btnLogin)
    ImageView image;
    @BindView(R.id.constraintSignUp)
    LinearLayout constraintSignUp;
    @BindView(R.id.inputUsername)
    EditText inputUsername;
    @BindView(R.id.inputPassword)
    EditText inputPassword;
    @BindView(R.id.inputEmail)
    EditText inputEmail;
    @BindView(R.id.inputContact)
    EditText inputContact;
    @BindView(R.id.inputAddress)
    EditText inputAddress;
    @BindView(R.id.txtErroMsg)
    TextView txtErrorMsg;
    @BindView(R.id.fabSignUp)
    Button fabSignUp;
    @BindView(R.id.relEroorMsg)
    RelativeLayout relErrorMsg;

    private ProgressDialogFragment progressDialogFragment;

    public SignupFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_signup, container, false);
        ButterKnife.bind(this, view);
        setTypeFaceLayout();
        progressDialogFragment = new ProgressDialogFragment();
        inputUsername.setOnFocusChangeListener(SignupFragment.this);
        inputPassword.setOnFocusChangeListener(SignupFragment.this);
        inputEmail.setOnFocusChangeListener(SignupFragment.this);
        inputContact.setOnFocusChangeListener(SignupFragment.this);
        inputAddress.setOnFocusChangeListener(SignupFragment.this);
        return view;

    }

    /*
     Adding Type face to @Field
    */
    private void setTypeFaceLayout() {

        Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/JosefinSans-Regular.ttf");
        fabSignUp.setTypeface(typeface);
        inputUsername.setTypeface(typeface);
        inputPassword.setTypeface(typeface);
        inputEmail.setTypeface(typeface);
        inputContact.setTypeface(typeface);
        inputAddress.setTypeface(typeface);
        txtErrorMsg.setTypeface(typeface);

    }

    @OnClick({R.id.btnLogin, R.id.fabSignUp})
    public void onBtnClick(View view) {
        switch (view.getId()) {

            case R.id.btnLogin:
                openLoginFragment();
                break;

            case R.id.fabSignUp:
                if (isValid()) {
                    signUpTask();
                }
                break;
            default:
                break;
        }
    }

    private void openLoginFragment() {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, new LoginFragment()).commit();
    }

    private boolean isValid() {
        boolean isValid = true;

        String message = "";
        if (inputUsername.getText().toString().isEmpty()) {
            isValid = false;
            message = "Please enter username";
        } else if (inputPassword.getText().toString().isEmpty()) {
            isValid = false;
            message = "Please enter password";
        } else if (inputEmail.getText().toString().isEmpty()) {
            isValid = false;
            message = "Please enter email";
        } else if (inputContact.getText().toString().isEmpty()) {
            isValid = false;
            message = "Please enter contact number";
        } else if (inputAddress.getText().toString().isEmpty()) {
            isValid = false;
            message = "Please enter address";
        }

        if (!isValid) {
            relErrorMsg.setVisibility(View.VISIBLE);
            txtErrorMsg.setText(message);

        }
        return isValid;
    }

    /**
     * Method to sign up user
     */
    private void signUpTask() {

        LoginPresenter loginPresenter = new LoginPresenterImpl(this);
        loginPresenter.onSignUpCall(inputUsername.getText().toString(), inputPassword.getText().toString(), inputEmail.getText().toString(), inputContact.getText().toString(), inputAddress.getText().toString());
    }

    @Override
    public void showProgress() {
        if (progressDialogFragment != null) {
            progressDialogFragment.show(getFragmentManager(), getResources().getString(R.string.msg_progress_dialog));
        }
    }

    @Override
    public void hideProgress() {
        if (progressDialogFragment != null) {
            progressDialogFragment.dismiss();
        }
    }

    @Override
    public void onLoginSuccess(LoginEntity loginResponse) {
        openLoginFragment();
        Toast.makeText(getActivity(), loginResponse.getMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailedMessage(String message) {
        relErrorMsg.setVisibility(View.VISIBLE);
        txtErrorMsg.setText(message);
        hideProgress();
    }

    @Override
    public void onServiceError(String message) {
        ShowAlertDialog.showAlert(getActivity(), message, "OK");
        hideProgress();
    }

    protected void setupParent(View view) {
        //Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard();
                    return false;
                }
            });
        }
        //If a layout container, iterate over children
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupParent(innerView);
            }
        }
    }

    private void hideSoftKeyboard() {
        InputMethodManager inputMethodManager =
                (InputMethodManager) getActivity().getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(
                getActivity().getCurrentFocus().getWindowToken(), 0);
    }

    @Override
    public void onFocusChange(View view, boolean b) {
        switch (view.getId()) {
            case R.id.inputUsername:
                if (!b) {
                    setupParent(view);
                }
                break;
            case R.id.inputPassword:
                if (!b) {
                    setupParent(view);
                }
                break;
            case R.id.inputEmail:
                if (!b) {
                    setupParent(view);
                }
                break;
            case R.id.inputContact:
                if (!b) {
                    setupParent(view);
                }
                break;
            case R.id.inputAddress:
                if (!b) {
                    setupParent(view);
                }
                break;
            default:
                break;
        }
    }
}
