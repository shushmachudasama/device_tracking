package com.eshiksa.devicetracking.viewimpl.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.eshiksa.devicetracking.R;
import com.eshiksa.devicetracking.pojo.order.ListOfOrder;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author by SHUSHMA on 19/5/18.
 */
public class OrderStatusAdapter extends RecyclerView.Adapter<OrderStatusAdapter.OrderHolder> {

    private List<ListOfOrder> mOrderStatusList;
    private Activity mContext;

    public OrderStatusAdapter(List<ListOfOrder> mOrderStatusList, Activity mContext) {
        this.mOrderStatusList = mOrderStatusList;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public OrderHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_order_status, parent, false);
        return new OrderHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull OrderHolder holder, int position) {

        ListOfOrder listOfOrder = mOrderStatusList.get(position);
        holder.createdDate.setText(listOfOrder.getCreatedDate());
        holder.numberOfDevices.setText(listOfOrder.getNumberOfDevices());
        holder.txnAmount.setText(listOfOrder.getTxnAmount().toString());
        holder.txtOrderStatus.setText(listOfOrder.getOrderStatus());

    }

    @Override
    public int getItemCount() {
        return mOrderStatusList.size();
    }

    public class OrderHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.createdDate)
        TextView createdDate;
        @BindView(R.id.numberOfDevices)
        TextView numberOfDevices;
        @BindView(R.id.txnAmount)
        TextView txnAmount;
        @BindView(R.id.txtOrderStatus)
        TextView txtOrderStatus;
//        @BindView(R.id.imgStatusIcon)
//        ImageView imgStatusIcon;

        public OrderHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
