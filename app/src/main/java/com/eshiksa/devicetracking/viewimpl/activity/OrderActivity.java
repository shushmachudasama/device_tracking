package com.eshiksa.devicetracking.viewimpl.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.eshiksa.devicetracking.R;
import com.eshiksa.devicetracking.pojo.order.OrderEntity;
import com.eshiksa.devicetracking.presenter.OrderPresenter;
import com.eshiksa.devicetracking.presenterimpl.OrderPresenterImpl;
import com.eshiksa.devicetracking.serviceimpl.ApiClient;
import com.eshiksa.devicetracking.serviceimpl.ApiInterface;
import com.eshiksa.devicetracking.util.Constant;
import com.eshiksa.devicetracking.util.SharePrefrancClass;
import com.eshiksa.devicetracking.util.ShowAlertDialog;
import com.eshiksa.devicetracking.view.OrderView;
import com.eshiksa.devicetracking.viewimpl.fragment.ProgressDialogFragment;
import com.google.gson.Gson;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderActivity extends AppCompatActivity implements OrderView {

    @BindView(R.id.inputFirstName)
    EditText inputFirstName;
    @BindView(R.id.inputLastName)
    EditText inputLastName;
    @BindView(R.id.inputEmail)
    EditText inputEmail;
    @BindView(R.id.inputAddress1)
    EditText inputAddress1;
    @BindView(R.id.inputAddress2)
    EditText inputAddress2;
    @BindView(R.id.inputCity)
    EditText inputCity;
    @BindView(R.id.inputState)
    EditText inputState;
    @BindView(R.id.inputPincode)
    EditText inputPincode;
    @BindView(R.id.inputQuantity)
    EditText inputQuantity;
    @BindView(R.id.inputPhoneNumber)
    EditText inputPhoneNumber;
    @BindView(R.id.inputCountry)
    EditText inputCountry;
    @BindView(R.id.btnSaveOrder)
    Button btnSaveOrder;
    @BindView(R.id.imageBack)
    ImageView imageBack;

    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private ProgressDialogFragment progressDialogFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);
        ButterKnife.bind(this);
        progressDialogFragment = new ProgressDialogFragment();
        setTypeFaceLayout();
    }

    private void setTypeFaceLayout() {
        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/JosefinSans-Regular.ttf");
        txtTitle.setTypeface(typeface);
        inputFirstName.setTypeface(typeface);
        inputLastName.setTypeface(typeface);
        inputEmail.setTypeface(typeface);
        inputAddress1.setTypeface(typeface);
        inputAddress2.setTypeface(typeface);
        inputCity.setTypeface(typeface);
        inputState.setTypeface(typeface);
        inputPincode.setTypeface(typeface);
        inputQuantity.setTypeface(typeface);
        inputPhoneNumber.setTypeface(typeface);
        inputCountry.setTypeface(typeface);
        btnSaveOrder.setTypeface(typeface);
    }

    /**
     * Method to save order
     */
    private void saveOrder() {

        OrderEntity orderEntity = new OrderEntity();

        orderEntity.setFirstName(inputFirstName.getText().toString());
        orderEntity.setLastName(inputLastName.getText().toString());
        orderEntity.setAddress1(inputAddress1.getText().toString());
        orderEntity.setAddress2(inputAddress2.getText().toString());
        orderEntity.setCity(inputCity.getText().toString());
        orderEntity.setCountry(inputCountry.getText().toString());
        orderEntity.setEmail(inputEmail.getText().toString());
        orderEntity.setPhoneNumber(inputPhoneNumber.getText().toString());
        orderEntity.setPinCode(inputPincode.getText().toString());
        orderEntity.setNumberOfDevices(inputQuantity.getText().toString());
        orderEntity.setUsername(SharePrefrancClass.getInstance(this).getPref(Constant.USERNAME));
        orderEntity.setState(inputState.getText().toString());

        OrderPresenter orderPresenter = new OrderPresenterImpl(this);
        orderPresenter.onOrderCall(orderEntity);
    }

    @OnClick({R.id.btnSaveOrder,
            R.id.imageBack})
    public void placeOrder(View view) {

        switch (view.getId()) {
            case R.id.btnSaveOrder:
                if (isValid())
                    saveOrder();
                break;
            case R.id.imageBack:
                onBackPressed();
                break;

        }

    }

    private boolean isValid() {
        boolean isValid = true;
        return isValid;
    }

    @Override
    public void showProgress() {
        if (progressDialogFragment != null) {
            progressDialogFragment.show(this.getFragmentManager(), getResources().getString(R.string.msg_progress_dialog));
        }
    }

    @Override
    public void hideProgress() {
        if (progressDialogFragment != null) {
            progressDialogFragment.dismiss();
        }
    }

    @Override
    public void onOrderSuccess(OrderEntity orderEntity) {

        String orderID = orderEntity.getOrderID();
        String username = SharePrefrancClass.getInstance(this).getPref(Constant.USERNAME);
        SharePrefrancClass.getInstance(this).savePref(Constant.ORDERID, orderEntity.getOrderID());
        gotopaymentGetway(orderID,username);
        Log.d("ORDERENTITY",new Gson().toJson(orderEntity));

    }

    private void gotopaymentGetway(String orderID, String username) {
        ApiInterface apiInterface = ApiClient.getClient(null).create(ApiInterface.class);
        HashMap<String,Object> hashMap = new HashMap<>();
        hashMap.put("orderId",orderID);
        hashMap.put("username",username);

        Call<OrderEntity> call = apiInterface.gotopg(hashMap);
        call.enqueue(new Callback<OrderEntity>() {
            @Override
            public void onResponse(Call<OrderEntity> call, Response<OrderEntity> response) {
                if (response.body()!=null){
                    OrderEntity entity = response.body();
                    if (entity.getStatus()){
                        Intent intent = new Intent(OrderActivity.this, WebViewActivity.class);
                        intent.putExtra("URL", entity.getRedirectUrl());
                        startActivity(intent);
                    } else {
                        ShowAlertDialog.showAlert(OrderActivity.this,getApplication().getResources().getString(R.string.something_went_wrong),"OK");
                    }
                }
            }

            @Override
            public void onFailure(Call<OrderEntity> call, Throwable t) {
              ShowAlertDialog.showAlert(OrderActivity.this,getApplication().getResources().getString(R.string.message_oops),"OK");
            }
        });
    }

    @Override
    public void onFailedMessage(String message) {
        ShowAlertDialog.showAlert(this, message, "OK");
        hideProgress();
    }

    @Override
    public void onServiceError(String message) {
        ShowAlertDialog.showAlert(this, message, "OK");
        hideProgress();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
