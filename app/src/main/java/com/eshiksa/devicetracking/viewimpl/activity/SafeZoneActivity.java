package com.eshiksa.devicetracking.viewimpl.activity;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.eshiksa.devicetracking.R;
import com.eshiksa.devicetracking.pojo.safezone.Devicezone;
import com.eshiksa.devicetracking.pojo.safezone.SafeZoneEntity;
import com.eshiksa.devicetracking.presenter.ModifyZonePresenter;
import com.eshiksa.devicetracking.presenterimpl.ModifyZonePresenterImpl;
import com.eshiksa.devicetracking.util.Constant;
import com.eshiksa.devicetracking.util.SharePrefrancClass;
import com.eshiksa.devicetracking.util.ShowAlertDialog;
import com.eshiksa.devicetracking.view.ModifyZoneView;
import com.eshiksa.devicetracking.viewimpl.adapter.SafeZoneAdapter;
import com.eshiksa.devicetracking.viewimpl.fragment.ProgressDialogFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SafeZoneActivity extends AppCompatActivity implements
        SafeZoneAdapter.OnZoneClickListener, ModifyZoneView {

    private List<Devicezone> zones;
    private int distance = 0;
    private int modifyPosition;
    private int deletePosition;
    private Dialog modifyDialog;
    private SafeZoneAdapter safeZoneAdapter;
    private ProgressDialogFragment progressDialogFragment;

    @BindView(R.id.recyclerZones)
    RecyclerView recyclerZones;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_safe_zone);
        ButterKnife.bind(SafeZoneActivity.this);
        progressDialogFragment = new ProgressDialogFragment();

        zones = new ArrayList<>();
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            zones = bundle.getParcelableArrayList("zones");
        }

        if (zones.size() > 0) {

            safeZoneAdapter = new SafeZoneAdapter(zones, SafeZoneActivity.this, this);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(SafeZoneActivity.this);
            recyclerZones.setLayoutManager(layoutManager);
            recyclerZones.setAdapter(safeZoneAdapter);
        }

    }

    @OnClick({R.id.imageBack})
    public void safeZones(View view) {

        switch (view.getId()) {

            case R.id.imageBack:
                onBackPressed();
                break;
        }

    }

    @Override
    public void onZoneClick(final Devicezone devicezone, int position) {

        modifyPosition = position;
        modifyDialog = new Dialog(SafeZoneActivity.this);
        modifyDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        modifyDialog.setContentView(R.layout.modify_zone);
        modifyDialog.setCancelable(false);
        modifyDialog.show();

        ImageView cancel = modifyDialog.findViewById(R.id.cancel_modify);
        final EditText altPlaceName = modifyDialog.findViewById(R.id.place_nickname);
        Button btn_save = modifyDialog.findViewById(R.id.btn_modify);
        SeekBar seekBar = modifyDialog.findViewById(R.id.seekBarDist);
        final TextView tvSeekDist = modifyDialog.findViewById(R.id.prevSeekDistance);
        tvSeekDist.setText(devicezone.getDistance() + "m");
        seekBar.setProgress(devicezone.getDistance());
        altPlaceName.setText(devicezone.getAlternatePlaceName());

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                int incrementBy = 20;
                progress = ((int) Math.round(progress / incrementBy)) * incrementBy;
                seekBar.setProgress(progress);
                tvSeekDist.setText(progress + "m");
                distance = progress;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String altPlace = altPlaceName.getText().toString();
                if (!altPlace.isEmpty()) {
                    SafeZoneEntity safeZoneEntity = new SafeZoneEntity();
                    safeZoneEntity.setDistance(String.valueOf(distance));
                    safeZoneEntity.setAlternatePlaceName(altPlace);
                    safeZoneEntity.setSafeZoneId(devicezone.getId());
                    safeZoneEntity.setUsername(SharePrefrancClass.getInstance(SafeZoneActivity.this).getPref(Constant.USERNAME));
                    safeZoneEntity.setDeviceNumber(devicezone.getDeviceNumber());
                    safeZoneEntity.setPlaceName(devicezone.getPlaceName());
                    safeZoneEntity.setLatitude(String.valueOf(devicezone.getLatitude()));
                    safeZoneEntity.setLongitude(String.valueOf(devicezone.getLongitude()));

                    modifySafeZoneTask(safeZoneEntity);
                } else {
                    Toast.makeText(SafeZoneActivity.this, "Please enter alternate name for place.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                modifyDialog.dismiss();
            }
        });

    }

    /**
     * Method to modify safe zone alternate name and distance
     *
     * @param safeZoneEntity
     */
    private void modifySafeZoneTask(SafeZoneEntity safeZoneEntity) {
        ModifyZonePresenter modifyZonePresenter = new ModifyZonePresenterImpl(this);
        modifyZonePresenter.onModifyZoneCall(safeZoneEntity);
    }

    @Override
    public void onDeleteZoneClick(final Devicezone devicezone, int position) {

        deletePosition = position;
        modifyDialog = new Dialog(SafeZoneActivity.this);
        modifyDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        modifyDialog.setContentView(R.layout.delete_zone);
        modifyDialog.setCancelable(false);
        modifyDialog.show();

        ImageView cancel = modifyDialog.findViewById(R.id.cancel_delete);
        Button btn_del = modifyDialog.findViewById(R.id.btn_del_zone);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                modifyDialog.dismiss();
            }
        });

        btn_del.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               deleteZoneTask(devicezone.getId());
            }
        });

    }

    /**
     * Method to delete zone
     * @param id
     */
    private void deleteZoneTask(Integer id) {
        ModifyZonePresenter modifyZonePresenter = new ModifyZonePresenterImpl(this);
        modifyZonePresenter.onDeleteZoneCall(String.valueOf(id));
    }

    @Override
    public void showProgress() {
        if (progressDialogFragment != null) {
            progressDialogFragment.show(this.getFragmentManager(), getResources().getString(R.string.msg_progress_dialog));
        }
    }

    @Override
    public void hideProgress() {
        if (progressDialogFragment != null) {
            progressDialogFragment.dismiss();
        }
    }

    @Override
    public void onModifyZoneSuccess(SafeZoneEntity safeZoneEntity) {
        modifyDialog.dismiss();
        safeZoneAdapter.notifyItemChanged(modifyPosition);
        safeZoneAdapter.notifyDataSetChanged();
        Toast.makeText(this, "Safe zone updated successfully.", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDeleteZoneSuccess(SafeZoneEntity safeZoneEntity) {
        modifyDialog.dismiss();
        zones.remove(deletePosition);
        safeZoneAdapter.notifyDataSetChanged();
        Toast.makeText(this, "Safe zone deleted successfully.", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onFailedMessage(String message) {
        modifyDialog.dismiss();
        ShowAlertDialog.showAlert(this, message, "OK");
        hideProgress();
    }

    @Override
    public void onServiceError(String message) {
        modifyDialog.dismiss();
        ShowAlertDialog.showAlert(this, message, "OK");
        hideProgress();
    }
}
