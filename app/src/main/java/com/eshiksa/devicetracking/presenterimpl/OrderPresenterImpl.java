package com.eshiksa.devicetracking.presenterimpl;

import com.eshiksa.devicetracking.pojo.LoginEntity;
import com.eshiksa.devicetracking.pojo.order.OrderEntity;
import com.eshiksa.devicetracking.presenter.OrderPresenter;
import com.eshiksa.devicetracking.service.OrderService;
import com.eshiksa.devicetracking.serviceimpl.activity.OrderServiceImpl;
import com.eshiksa.devicetracking.view.OrderView;

/**
 * @author by SHUSHMA on 24/4/18.
 */

public class OrderPresenterImpl implements OrderPresenter {
    
    private OrderView orderView;

    public OrderPresenterImpl(OrderView orderView) {
        this.orderView = orderView;
    }

    @Override
    public void onPrepareCall() {
        if (orderView != null)
            orderView.showProgress();
    }

    @Override
    public void onOrderCall(OrderEntity orderEntity) {
        OrderService orderService = new OrderServiceImpl(this);
        orderService.orderCall(orderEntity);
    }

    @Override
    public void onOrderSuccess(OrderEntity orderEntity) {
        if (orderView != null) {
            orderView.hideProgress();
            orderView.onOrderSuccess(orderEntity);
        }
    }

    @Override
    public void onOrderFailure(String message) {
        orderView.onFailedMessage(message);
    }

    @Override
    public void onOrderError(int requestCode, String message) {
        if (orderView != null) {
            orderView.hideProgress();
            orderView.onServiceError(message);
        }
    }
}
