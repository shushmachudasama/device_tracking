
package com.eshiksa.devicetracking.pojo.plan;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DevicePlansVO implements Parcelable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("planType")
    @Expose
    private String planType;
    @SerializedName("planAmount")
    @Expose
    private Integer planAmount;
    @SerializedName("discount")
    @Expose
    private Integer discount;
    @SerializedName("offer")
    @Expose
    private String offer;
    public final static Creator<DevicePlansVO> CREATOR = new Creator<DevicePlansVO>() {


        @SuppressWarnings({
                "unchecked"
        })
        public DevicePlansVO createFromParcel(Parcel in) {
            return new DevicePlansVO(in);
        }

        public DevicePlansVO[] newArray(int size) {
            return (new DevicePlansVO[size]);
        }

    };

    protected DevicePlansVO(Parcel in) {
        this.id = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.planType = ((String) in.readValue((String.class.getClassLoader())));
        this.planAmount = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.discount = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.offer = ((String) in.readValue((String.class.getClassLoader())));
    }

    public DevicePlansVO() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPlanType() {
        return planType;
    }

    public void setPlanType(String planType) {
        this.planType = planType;
    }

    public Integer getPlanAmount() {
        return planAmount;
    }

    public void setPlanAmount(Integer planAmount) {
        this.planAmount = planAmount;
    }

    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public String getOffer() {
        return offer;
    }

    public void setOffer(String offer) {
        this.offer = offer;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(planType);
        dest.writeValue(planAmount);
        dest.writeValue(discount);
        dest.writeValue(offer);
    }

    public int describeContents() {
        return 0;
    }

}
