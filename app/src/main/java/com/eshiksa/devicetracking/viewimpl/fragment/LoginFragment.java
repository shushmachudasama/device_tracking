package com.eshiksa.devicetracking.viewimpl.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.eshiksa.devicetracking.R;
import com.eshiksa.devicetracking.pojo.LoginEntity;
import com.eshiksa.devicetracking.presenter.LoginPresenter;
import com.eshiksa.devicetracking.presenterimpl.LoginPresenterImpl;
import com.eshiksa.devicetracking.util.Constant;
import com.eshiksa.devicetracking.util.SharePrefrancClass;
import com.eshiksa.devicetracking.util.ShowAlertDialog;
import com.eshiksa.devicetracking.util.Utility;
import com.eshiksa.devicetracking.view.LoginView;
import com.eshiksa.devicetracking.viewimpl.activity.HomeActivity;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginFragment extends Fragment implements LoginView, View.OnFocusChangeListener {

    private static final String TAG = LoginFragment.class.getSimpleName();
    private static final int REQUEST_CHECK_SETTINGS = 43;
    @BindView(R.id.constraintLogin)
    LinearLayout constraintLogin;
    @BindView(R.id.inputUsername)
    EditText inputUsername;
    @BindView(R.id.inputPassword)
    EditText inputPassword;
    @BindView(R.id.btn_login)
    Button btnSignIN;
    @BindView(R.id.txtSignUp)
    TextView txtSignUp;
    @BindView(R.id.txtForgotPassword)
    TextView txtForgotPassword;
    @BindView(R.id.txtErrorMsg)
    TextView txtErrorMsg;
    @BindView(R.id.relEroorMsg)
    RelativeLayout relEroorMsg;

    private ProgressDialogFragment progressDialogFragment;
    private Utility utility;

    public LoginFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_login, container, false);
        ButterKnife.bind(this, view);
        setTypeFaceLayout();
        inputUsername.setOnFocusChangeListener(LoginFragment.this);
        inputPassword.setOnFocusChangeListener(LoginFragment.this);
        progressDialogFragment = new ProgressDialogFragment();
        utility = new Utility(getActivity());
        checkForPermission();
        //utility.checkGps();
        checkForGps();
        return view;
    }

    private void setTypeFaceLayout() {
        Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/JosefinSans-Regular.ttf");
        btnSignIN.setTypeface(typeface);
        inputUsername.setTypeface(typeface);
        inputPassword.setTypeface(typeface);
        txtSignUp.setTypeface(typeface);
        txtForgotPassword.setTypeface(typeface);
    }

    @OnClick({R.id.txtSignUp, R.id.btn_login, R.id.txtForgotPassword})
    public void onClickBtn(View view) {

        switch (view.getId()) {

            case R.id.txtSignUp:
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.fragment_container, new SignupFragment()).commit();
                break;

            case R.id.txtForgotPassword:
                FragmentTransaction trans = getFragmentManager().beginTransaction();
                trans.replace(R.id.fragment_container, new ForgotPasswordFragment()).commit();
                break;

            case R.id.btn_login:
                if (isValid()) {
                    if (isInternetAvailable()) {
                        loginTask();
                    }
                }
                break;

            default:
                break;
        }
    }

    private boolean isValid() {

        boolean isValid = true;
        String message = "";

        String username = inputUsername.getText().toString();
        String password = inputPassword.getText().toString();

        if (username.isEmpty()) {
            message = "Please enter username";
            isValid = false;

        } else if (password.isEmpty()) {
            message = "Please enter password";
            isValid = false;
        }

        if (!isValid) {
            relEroorMsg.setVisibility(View.VISIBLE);
            txtErrorMsg.setText(message);
        }

        return isValid;

    }

    private void loginTask() {
        relEroorMsg.setVisibility(View.INVISIBLE);
        LoginPresenter loginPresenter = new LoginPresenterImpl(this);
        loginPresenter.onLoginCall(inputUsername.getText().toString(), inputPassword.getText().toString());

    }

    protected void setupParent(View view) {
        //Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard();
                    return false;
                }
            });
        }
        //If a layout container, iterate over children
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupParent(innerView);
            }
        }
    }

    private int PERMISSION_ALL = 1;

    private boolean isInternetAvailable() {

        if (!utility.internetStatus()) {
            Snackbar.make(constraintLogin, "Please check internet connection", Snackbar.LENGTH_SHORT).show();
            return false;
        }
        utility = new Utility(getActivity());
        return true;
    }

    private void checkForGps() {
        displayLocationSettingsRequest(getActivity());
    }

    private void displayLocationSettingsRequest(Context context) {
        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API).build();
        googleApiClient.connect();

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(10000 / 2);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        Log.i(TAG, "All location settings are satisfied.");
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        Log.i(TAG, "Location settings are not satisfied. Show the user a dialog to upgrade location settings ");

                        try {
                            // Show the dialog by calling startResolutionForResult(), and check the result
                            // in onActivityResult().
                            status.startResolutionForResult(getActivity(), REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            Log.i(TAG, "PendingIntent unable to execute request.");
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        Log.i(TAG, "Location settings are inadequate, and cannot be fixed here. Dialog not created.");
                        break;
                }
            }
        });
    }

    private void checkForPermission() {

        String[] PERMISSIONS = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};

        if (!utility.hasPermissions(getActivity(), PERMISSIONS)) {
            ActivityCompat.requestPermissions(getActivity(), PERMISSIONS, PERMISSION_ALL);
        }
    }

    private void hideSoftKeyboard() {
        InputMethodManager inputMethodManager =
                (InputMethodManager) getActivity().getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(
                getActivity().getCurrentFocus().getWindowToken(), 0);
    }

    @Override
    public void showProgress() {
        if (progressDialogFragment != null) {
            progressDialogFragment.show(getFragmentManager(), getResources().getString(R.string.msg_progress_dialog));
        }
    }

    @Override
    public void hideProgress() {
        if (progressDialogFragment != null)
            progressDialogFragment.dismiss();
    }

    @Override
    public void onLoginSuccess(LoginEntity loginResponse) {

        SharePrefrancClass.getInstance(getActivity()).savePref(Constant.USERNAME, inputUsername.getText().toString());
        startActivity(new Intent(getActivity(), HomeActivity.class));
        getActivity().finish();

    }

    @Override
    public void onFailedMessage(String message) {
        relEroorMsg.setVisibility(View.VISIBLE);
        hideProgress();
    }

    @Override
    public void onServiceError(String message) {
        ShowAlertDialog.showAlert(getActivity(), message, "OK");
        hideProgress();
    }

    @Override
    public void onFocusChange(View view, boolean b) {
        switch (view.getId()) {
            case R.id.inputUsername:
                if (!b) {
                    setupParent(view);
                }
                break;
            case R.id.inputPassword:
                if (!b) {
                    setupParent(view);
                }
                break;
            default:
                break;
        }
    }
}
