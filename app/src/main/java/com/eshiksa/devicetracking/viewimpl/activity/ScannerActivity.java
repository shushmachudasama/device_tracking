package com.eshiksa.devicetracking.viewimpl.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseArray;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.TextView;
import android.widget.Toast;

import com.eshiksa.devicetracking.R;
import com.eshiksa.devicetracking.pojo.ScanData;
import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class ScannerActivity extends AppCompatActivity {

    SurfaceView surfaceView;
    TextView txtBarcodeValue;
    private BarcodeDetector barcodeDetector;
    private CameraSource cameraSource;
    private static final int REQUEST_CAMERA_PERMISSION = 201;
    String intentData = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scanner);

        initViews();
    }

    private void initViews() {
        txtBarcodeValue = findViewById(R.id.txtBarcodeValue);
        surfaceView = findViewById(R.id.surfaceView);
    }

    @Override
    protected void onResume() {
        super.onResume();
        initialiseDetectorsAndSources();
    }

    private void initialiseDetectorsAndSources() {

            Toast.makeText(getApplicationContext(), "Barcode scanner started", Toast.LENGTH_SHORT).show();

            barcodeDetector = new BarcodeDetector.Builder(this)
                    .setBarcodeFormats(Barcode.ALL_FORMATS)
                    .build();

            cameraSource = new CameraSource.Builder(this, barcodeDetector)
                    .setRequestedPreviewSize(1920, 1080)
                    .setAutoFocusEnabled(true) //you should add this feature
                    .build();

            surfaceView.getHolder().addCallback(new SurfaceHolder.Callback() {
                @Override
                public void surfaceCreated(SurfaceHolder holder) {
                    try {
                        if (ActivityCompat.checkSelfPermission(ScannerActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                            cameraSource.start(surfaceView.getHolder());
                        } else {
                            ActivityCompat.requestPermissions(ScannerActivity.this, new
                                    String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA_PERMISSION);
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                }

                @Override
                public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
                }

                @Override
                public void surfaceDestroyed(SurfaceHolder holder) {
                    cameraSource.stop();
                }
            });


            barcodeDetector.setProcessor(new Detector.Processor<Barcode>() {
                @Override
                public void release() {
                    Toast.makeText(getApplicationContext(), "To prevent memory leaks barcode scanner has been stopped", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void receiveDetections(Detector.Detections<Barcode> detections) {
                    final SparseArray<Barcode> barcodes = detections.getDetectedItems();
                    if (barcodes.size() != 0) {


                        txtBarcodeValue.post(new Runnable() {

                            @Override
                            public void run() {

                                if (barcodes.valueAt(0).email != null) {
                                    txtBarcodeValue.removeCallbacks(null);
                                    intentData = barcodes.valueAt(0).email.address;
//                                    txtBarcodeValue.setText(intentData);
                                    ScanData data = parseData(intentData);
                                    Intent returnIntent = new Intent();
                                    returnIntent.putExtra("SCAN_DATA", data);
                                    setResult(Activity.RESULT_OK, returnIntent);
                                    finish();
                                } else {
                                    intentData = barcodes.valueAt(0).displayValue;
//                                    txtBarcodeValue.setText(intentData);
                                    ScanData data = parseData(intentData);
                                    Intent returnIntent = new Intent();
                                    returnIntent.putExtra("SCAN_DATA", intentData);
                                    setResult(Activity.RESULT_OK, returnIntent);
                                    finish();
                                    if (intentData.contains("<?xml")) {
                                        ScanData dataa = parseData(intentData);
                                        Intent returnIntentt = new Intent();
                                        returnIntent.putExtra("SCAN_DATA", dataa);
                                        setResult(Activity.RESULT_OK, returnIntentt);
                                        finish();
                                    }
                                }
                            }
                        });

                    }
                }
            });
    }

    private ScanData parseData(String contents) {

//        String xml = contents.replaceAll(">\\s+<", "><").trim();
        String xml = contents.replaceAll("[^\\x20-\\x7e]", "");
        Log.e("XML: ", xml);
        DocumentBuilderFactory dbf =
                DocumentBuilderFactory.newInstance();
        try
        {
            DocumentBuilder db = dbf.newDocumentBuilder();
            byte[] bytes = Charset.forName("UTF-8").encode(xml).array();
            InputStream is = new ByteArrayInputStream(bytes);
            Document doc = db.parse(is);
            NodeList nList = doc.getElementsByTagName("PrintLetterBarcodeData");

            ScanData adharData = new ScanData();
            for (int i = 0; i < nList.getLength(); i++) {
                Node node = nList.item(i);
                Element element1 = (Element) node;
                adharData.setDeviceId(element1.getAttribute("id"));
                return adharData;
            }

        } catch (ParserConfigurationException e) {
            Log.e("Error: ", e.getMessage());
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;

//        try {
//            InputStream is = new ByteArrayInputStream(contents.getBytes("UTF-8"));
//            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
//            DocumentBuilder dbBuilder = dbFactory.newDocumentBuilder();
//            Document doc = dbBuilder.parse(is);
//            Element element = doc.getDocumentElement();
//            element.normalize();
//            NodeList nList = doc.getElementsByTagName("PrintLetterBarcodeData");
//            ScanData adharData = new ScanData();
//            for (int i = 0; i < nList.getLength(); i++) {
//                Node node = nList.item(i);
//                Element element1 = (Element) node;
//                adharData.setDeviceId(element1.getAttribute("id"));
////                adharData.setAdharNo(element1.getAttribute("uid"));
////                adharData.setAddress1(element1.getAttribute("lm"));
////                adharData.setAddress2(element1.getAttribute("loc"));
////                adharData.setCity(element1.getAttribute("vtc"));
////                adharData.setState(element1.getAttribute("state"));
////                adharData.setDistrict(element1.getAttribute("dist"));
////                adharData.setPincode(element1.getAttribute("pc"));
////                adharData.setGender(element1.getAttribute("gender"));
////                adharData.setDob(element1.getAttribute("dob"));
//                return adharData;
//            }
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        } catch (ParserConfigurationException e) {
//            e.printStackTrace();
//        } catch (SAXException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        return null;
    }
}
