package com.eshiksa.devicetracking.serviceimpl.activity;

import com.eshiksa.devicetracking.pojo.safezone.SafeZoneEntity;
import com.eshiksa.devicetracking.presenter.ModifyZonePresenter;
import com.eshiksa.devicetracking.service.ModifyZoneService;
import com.eshiksa.devicetracking.serviceimpl.ApiClient;
import com.eshiksa.devicetracking.serviceimpl.ApiInterface;

import java.net.HttpURLConnection;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * @author by SHUSHMA on 16/5/18.
 */

public class ModifyZoneServiceImpl implements ModifyZoneService {

    private ModifyZonePresenter modifyZonePresenter;

    public ModifyZoneServiceImpl(ModifyZonePresenter modifyZonePresenter) {
        this.modifyZonePresenter = modifyZonePresenter;
    }

    @Override
    public void modifyZoneCall(SafeZoneEntity safeZoneEntity) {
        modifyZonePresenter.onPrepareCall();

        ApiInterface apiInterface = ApiClient.getClient(null).create(ApiInterface.class);
        Call<SafeZoneEntity> loginEntityCall = apiInterface.updateSafeZone(safeZoneEntity);
        loginEntityCall.enqueue(new Callback<SafeZoneEntity>() {
            @Override
            public void onResponse(Call<SafeZoneEntity> call, Response<SafeZoneEntity> response) {

                if (response.body() != null) {

                    SafeZoneEntity safeZoneEntity = response.body();

                    if (safeZoneEntity.getStatus()) {
                        modifyZonePresenter.onModifyZoneSuccess(safeZoneEntity);
                    } else {
                        modifyZonePresenter.onModifyZoneFailure(safeZoneEntity.getMessage());
                    }

                } else {
                    modifyZonePresenter.onModifyZoneFailure(response.message());
                }

            }

            @Override
            public void onFailure(Call<SafeZoneEntity> call, Throwable t) {
                modifyZonePresenter.onModifyZoneError(HttpURLConnection.HTTP_INTERNAL_ERROR, "Oops! Some problem seems to have happened. Please try again");
            }
        });
    }

    @Override
    public void deleteZoneCall(String zoneId) {
        modifyZonePresenter.onPrepareCall();

        HashMap<String, Object> map = new HashMap<>();
        map.put("safeZoneId", zoneId);

        ApiInterface apiInterface = ApiClient.getClient(null).create(ApiInterface.class);
        Call<SafeZoneEntity> loginEntityCall = apiInterface.deleteSafeZone(map);
        loginEntityCall.enqueue(new Callback<SafeZoneEntity>() {
            @Override
            public void onResponse(Call<SafeZoneEntity> call, Response<SafeZoneEntity> response) {

                if (response.body() != null) {

                    SafeZoneEntity safeZoneEntity = response.body();

                    if (safeZoneEntity.getStatus()) {
                        modifyZonePresenter.onDeleteZoneSuccess(safeZoneEntity);
                    } else {
                        modifyZonePresenter.onModifyZoneFailure(safeZoneEntity.getMessage());
                    }

                } else {
                    modifyZonePresenter.onModifyZoneFailure(response.message());
                }

            }

            @Override
            public void onFailure(Call<SafeZoneEntity> call, Throwable t) {
                modifyZonePresenter.onModifyZoneError(HttpURLConnection.HTTP_INTERNAL_ERROR, "Oops! Some problem seems to have happened. Please try again");
            }
        });
    }
}
