package com.eshiksa.devicetracking.view;

import com.eshiksa.devicetracking.pojo.LoginEntity;

/**
 * @author by SHUSHMA on 23/4/18.
 */

public interface LoginView extends CommonView {

    void onLoginSuccess(LoginEntity loginResponse);

    void onFailedMessage(String message);

    void onServiceError(String status);

}
