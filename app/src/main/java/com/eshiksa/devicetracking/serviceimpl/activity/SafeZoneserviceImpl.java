package com.eshiksa.devicetracking.serviceimpl.activity;

import com.eshiksa.devicetracking.pojo.safezone.SafeZoneEntity;
import com.eshiksa.devicetracking.presenter.SafeZonePresenter;
import com.eshiksa.devicetracking.service.SafeZoneService;
import com.eshiksa.devicetracking.serviceimpl.ApiClient;
import com.eshiksa.devicetracking.serviceimpl.ApiInterface;

import java.net.HttpURLConnection;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * @author by SHUSHMA on 12/5/18.
 */

public class SafeZoneserviceImpl implements SafeZoneService {

    private SafeZonePresenter safeZonePresenter;

    public SafeZoneserviceImpl(SafeZonePresenter safeZonePresenter) {
        this.safeZonePresenter = safeZonePresenter;
    }

    @Override
    public void addSafeZoneCall(SafeZoneEntity safeZoneEntity) {
        safeZonePresenter.onPrepareCall();

        ApiInterface apiInterface = ApiClient.getClient(null).create(ApiInterface.class);
        Call<SafeZoneEntity> loginEntityCall = apiInterface.saveSafeZone(safeZoneEntity);
        loginEntityCall.enqueue(new Callback<SafeZoneEntity>() {
            @Override
            public void onResponse(Call<SafeZoneEntity> call, Response<SafeZoneEntity> response) {

                if (response.body() != null) {

                    SafeZoneEntity safeZoneEntity = response.body();

                    if (safeZoneEntity.getStatus()) {
                        safeZonePresenter.onAddSafeZoneSuccess(safeZoneEntity);
                    } else {
                        safeZonePresenter.onSafeZoneFailure(safeZoneEntity.getMessage());
                    }

                } else {
                    safeZonePresenter.onSafeZoneFailure(response.message());
                }

            }

            @Override
            public void onFailure(Call<SafeZoneEntity> call, Throwable t) {

                safeZonePresenter.onSafeZoneError(HttpURLConnection.HTTP_INTERNAL_ERROR, "Oops! Some problem seems to have happened. Please try again");
            }
        });

    }

    @Override
    public void safeZoneListCall(String username, String deviceId) {
        safeZonePresenter.onPrepareCall();

        HashMap<String, Object> map = new HashMap<>();
        map.put("userName", username);
        map.put("deviceNumber", deviceId);

        ApiInterface apiInterface = ApiClient.getClient(null).create(ApiInterface.class);
        Call<SafeZoneEntity> loginEntityCall = apiInterface.getSafeZoneList(map);
        loginEntityCall.enqueue(new Callback<SafeZoneEntity>() {
            @Override
            public void onResponse(Call<SafeZoneEntity> call, Response<SafeZoneEntity> response) {

                if (response.body() != null) {

                    SafeZoneEntity safeZoneEntity = response.body();

                    if (safeZoneEntity.getStatus()) {
                        safeZonePresenter.onSafeZoneListSuccess(safeZoneEntity);
                    } else {
                        safeZonePresenter.onSafeZoneFailure(safeZoneEntity.getMessage());
                    }

                } else {
                    safeZonePresenter.onSafeZoneFailure(response.message());
                }

            }

            @Override
            public void onFailure(Call<SafeZoneEntity> call, Throwable t) {
                safeZonePresenter.onSafeZoneError(HttpURLConnection.HTTP_INTERNAL_ERROR, "Oops! Some problem seems to have happened. Please try again");
            }
        });

    }

}
