package com.eshiksa.devicetracking.serviceimpl.activity;

import com.eshiksa.devicetracking.pojo.LoginEntity;
import com.eshiksa.devicetracking.presenter.LoginPresenter;
import com.eshiksa.devicetracking.service.LoginService;
import com.eshiksa.devicetracking.serviceimpl.ApiClient;
import com.eshiksa.devicetracking.serviceimpl.ApiInterface;

import java.net.HttpURLConnection;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * @author by SHUSHMA on 23/4/18.
 */

public class LoginServiceImpl implements LoginService {

    private LoginPresenter loginPresenter;

    public LoginServiceImpl(LoginPresenter loginPresenter) {
        this.loginPresenter = loginPresenter;
    }

    @Override
    public void onLoginServiceCall(String username, String password) {

        loginPresenter.onPrepareCall();

        HashMap<String, Object> loginMap = new HashMap<>();
        loginMap.put("username", username);
        loginMap.put("password", password);

        ApiInterface apiInterface = ApiClient.getClient(null).create(ApiInterface.class);
        Call<LoginEntity> loginEntityCall = apiInterface.loginUser(loginMap);
        loginEntityCall.enqueue(new Callback<LoginEntity>() {
            @Override
            public void onResponse(Call<LoginEntity> call, Response<LoginEntity> response) {

                if (response.body() != null) {

                    LoginEntity loginEntity = response.body();
                    if (loginEntity.getStatus()) {
                        loginPresenter.onLoginSuccess(loginEntity);
                    } else {
                        loginPresenter.onLoginFailure(loginEntity.getMessage());
                    }

                } else {
                    loginPresenter.onLoginFailure(response.message());
                }

            }

            @Override
            public void onFailure(Call<LoginEntity> call, Throwable t) {
                loginPresenter.onLoginError(HttpURLConnection.HTTP_INTERNAL_ERROR, "Oops! Some problem seems to have happened. Please try again");
            }

        });

    }

    @Override
    public void onEmailServiceCall(String email) {
        loginPresenter.onPrepareCall();

        HashMap<String, Object> emailMap = new HashMap<>();
        emailMap.put("email", email);

        ApiInterface apiInterface = ApiClient.getClient(null).create(ApiInterface.class);
        Call<LoginEntity> loginEntityCall = apiInterface.forgotPassword(emailMap);
        loginEntityCall.enqueue(new Callback<LoginEntity>() {
            @Override
            public void onResponse(Call<LoginEntity> call, Response<LoginEntity> response) {

                if (response.body() != null) {

                    LoginEntity loginEntity = response.body();
                    if (loginEntity.getStatus()) {
                        loginPresenter.onEmailSuccess(loginEntity);
                    } else {
                        loginPresenter.onLoginFailure(loginEntity.getMessage());
                    }

                } else {
                    loginPresenter.onLoginFailure(response.message());
                }

            }

            @Override
            public void onFailure(Call<LoginEntity> call, Throwable t) {
                loginPresenter.onLoginError(HttpURLConnection.HTTP_INTERNAL_ERROR, "Oops! Some problem seems to have happened. Please try again");
            }

        });

    }

    @Override
    public void onSignUpServiceCall(String username, String password, String email, String contact, String address) {

        loginPresenter.onPrepareCall();

        HashMap<String, Object> signUpMap = new HashMap<>();
        signUpMap.put("username", username);
        signUpMap.put("password", password);
        signUpMap.put("email", email);
        signUpMap.put("contactNumber", contact);
        signUpMap.put("address", address);

        ApiInterface apiInterface = ApiClient.getClient(null).create(ApiInterface.class);
        Call<LoginEntity> loginEntityCall = apiInterface.signUpUser(signUpMap);
        loginEntityCall.enqueue(new Callback<LoginEntity>() {
            @Override
            public void onResponse(Call<LoginEntity> call, Response<LoginEntity> response) {

                if (response.body() != null) {

                    LoginEntity loginEntity = response.body();
                    if (loginEntity.getStatus()) {
                        loginPresenter.onSignUpSuccess(loginEntity);
                    } else {
                        loginPresenter.onLoginFailure(loginEntity.getMessage());
                    }

                } else {
                    loginPresenter.onLoginFailure(response.message());
                }
            }

            @Override
            public void onFailure(Call<LoginEntity> call, Throwable t) {
                loginPresenter.onLoginError(HttpURLConnection.HTTP_INTERNAL_ERROR, "Oops! Some problem seems to have happened. Please try again");
            }
        });


    }
}
